  #include <Wire.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <SD.h>

//i2c Addresses
static const uint8_t LP55231_I2C = 0x32;
static const uint8_t TCA9548A_I2C = 0x70;
static const uint8_t TCS3472_I2C = 0x29;
static const uint8_t TSL2572_I2C = 0x39;
static const uint8_t VEML6070_I2C_H = 0x39; //for read operations
static const uint8_t VEML6070_I2C_L = 0x38; //for read and write operations
static const uint8_t VEML6070_ADDR_ARA = 0x0C; //Alert Resp Address (read to clear condition)

//LP5231 Registers
//Enable register
static const uint8_t LP55231_REG_CNTRL1 = 0x00;
//Reset register
static const uint8_t LP55231_REG_RESET = 0x3D;
//MISC register: [0] Clock source selection, [1] External clock detection, 
//[2] PWM cycle powersave enable, [3,4] Charge pump gain selection, 
//[5] Powersave mode enable, [6] Serial bus address auto increment enable, 
//[7] Variable D source selection)
static const uint8_t LP55231_REG_MISC = 0x36;
//PWM duty cycle control register
static const uint8_t LP55231_REG_D8_PWM = 0x1D; //UV
static const uint8_t LP55231_REG_D5_PWM = 0x1A; //green
static const uint8_t LP55231_REG_D6_PWM = 0x1B; //blue
static const uint8_t LP55231_REG_D7_PWM = 0x1C; //IR
static const uint8_t LP55231_REG_D9_PWM = 0x1E; //red
//output current control register
static const uint8_t LP55231_REG_D8_I_CTL = 0x2D; //UV
static const uint8_t LP55231_REG_D5_I_CTL = 0x2A; //green
static const uint8_t LP55231_REG_D6_I_CTL = 0x2B; //blue
static const uint8_t LP55231_REG_D7_I_CTL = 0x2C; //IR
static const uint8_t LP55231_REG_D9_I_CTL = 0x2E; //red
//Temperature measurement register
static const uint8_t LP55231_REG_TEMP_CTL = 0x3E; //control register for temperature measurement
static const uint8_t LP55231_REG_TEMP_DATA = 0x3F; //temperature measurement data
//LED test register (voltage measurement)
static const uint8_t LP55231_REG_VOLTAGE_CTL = 0x41; //control register for voltage measurement
static const uint8_t LP55231_REG_VOLTAGE_DATA = 0x42; //voltage measurement data

static const uint8_t LP55231_CHANNEL_G = 0x04; //green
static const uint8_t LP55231_CHANNEL_B = 0x05; //blue
static const uint8_t LP55231_CHANNEL_IR = 0x06; //IR
static const uint8_t LP55231_CHANNEL_UV = 0x07; //UV
static const uint8_t LP55231_CHANNEL_R = 0x08; //red

//VEML6070 Shutdown mode setting
static const uint8_t VEML6070_SD_DISABLE = 0x00;
static const uint8_t VEML6070_SD_ENABLE = 0x01;

//VEML6070 integration time settings
static const uint8_t VEML6070_INT_HALF = 0x00;
static const uint8_t VEML6070_INT_1 = 0x04;
static const uint8_t VEML6070_INT_2 = 0x08;
static const uint8_t VEML6070_INT_4 = 0x0C;

//VEML6070 current command register content
uint8_t _VEML6070_command_register = 0x00;

//TSL2572 registers
static const uint8_t TSL2572_ENABLE = 0x00; //power on/off, enable functions, interrupt settings
static const uint8_t TSL2572_ALS_TIME = 0x01; //ALS integration time
static const uint8_t TSL2572_CONFIG = 0x0D; //set wait long and ALS gain level (1/6 for 1X, 8X)
static const uint8_t TSL2572_ALS_GAIN = 0x0F; //ALS gain
static const uint8_t TSL2572_DATA = 0x14; //Data Register C0 (0x14, 0x15), Data Register C1 (0x16, 0x17)

//TSL2572 command settings
static const uint8_t TSL2572_AUTO_INCREMENT = 0xA0; //auto-increment register pointer
static const uint8_t TSL2572_COMMAND_BIT = 0x80; //command bit

//TSL2572 gain settings
static const uint8_t TSL2572_GAIN_1X = 0;
static const uint8_t TSL2572_GAIN_8X = 1;
static const uint8_t TSL2572_GAIN_16X = 2;
static const uint8_t TSL2572_GAIN_120X = 3;

//TSL2572 gain 1/6
bool _TSL2572_gainDivide_6 = false;

//TSL2572 integration time settings
static const uint8_t TSL2572_INT_TIME_63 = 256 - 23; //62.79 ms
static const uint8_t TSL2572_INT_TIME_125 = 256 - 46; //125.58 ms
static const uint8_t TSL2572_INT_TIME_250 = 256 - 92; //251.16 ms
static const uint8_t TSL2572_INT_TIME_500 = 256 - 183; //499.59 ms

//TCS3472 registers
static const uint8_t TCS3472_ENABLE = 0x00; //power on/off, enable functions, interrupt settings
static const uint8_t TCS3472_RGBC_TIME = 0x01; //RGBC integration time
static const uint8_t TCS3472_WAIT_TIME = 0x03; //Wait time register
static const uint8_t TCS3472_CONFIG = 0x0D; //wait long
static const uint8_t TCS3472_RGBC_GAIN = 0x0F; //RGBC gain
static const uint8_t TCS3472_CDATAL = 0x14; //clear channel data low byte
static const uint8_t TCS3472_CDATAH = 0x15; //clear channel data high byte
static const uint8_t TCS3472_RDATAL = 0x16; //red channel data low byte
static const uint8_t TCS3472_RDATAH = 0x17; //red channel data high byte
static const uint8_t TCS3472_GDATAL = 0x18; //green channel data low byte
static const uint8_t TCS3472_GDATAH = 0x19; //green channel data high byte
static const uint8_t TCS3472_BDATAL = 0x1A; //blue channel data low byte
static const uint8_t TCS3472_BDATAH = 0x1B; //blue channel data high byte

//TCS3472 command settings
static const uint8_t TCS3472_REPEATED_BYTE = 0x80;  //repeateadly read same register
static const uint8_t TCS3472_AUTO_INCREMENT = 0xA0; //auto-increment register pointer
static const uint8_t TCS3472_COMMAND_BIT = 0x80; //command bit

//TCS3472 gain settings
static const uint8_t TCS3472_GAIN_1X = 0;
static const uint8_t TCS3472_GAIN_4X = 1;
static const uint8_t TCS3472_GAIN_16X = 2;
static const uint8_t TCS3472_GAIN_60X = 3;

//TCS3472 integration time settings
static const uint8_t TCS3472_INT_TIME_63 = 256 - 26; //62.4 ms
static const uint8_t TCS3472_INT_TIME_125 = 256 - 52; //124.8 ms
static const uint8_t TCS3472_INT_TIME_250 = 256 - 104; //249.6 ms
static const uint8_t TCS3472_INT_TIME_500 = 256 - 208; //499.2 ms
//-------------------------------------------------------------------------------------------------------
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while(!Serial) continue;
  Wire.begin();

  TCA9548A_enableVEML6070();
  VEML6070_disable();
  TCA9548A_enableTSL2572();
  TSL2572_disable();
  TCA9548A_enableTCS3472();
  TCS3472_disable();
  TCA9548A_disable();
  LP55231_enable();
  LP55231_ledsOff();

  //inizialize SD Card
  if (!SD.begin(4)) {
    Serial.println("SD Card initialization failed!");
    //when initialisation failes, don't do anything more 
    while (1);
  }
  
  uint8_t intTime = TCS3472_INT_TIME_63;
  uint8_t gain = TCS3472_GAIN_1X;
  uint16_t pwm = 0, current = 0;
  uint16_t data;
  float voltage;
  int8_t temperatureLED;
  bool stopLoop = false;

  LP55231_enable();
  LP55231_ledsOff();
  TCA9548A_enableTCS3472();

  do{
  //for each integration time setting    
    for(gain; gain < 4; gain++){ 
    //for each gain setting ...
    TCS3472_enable(gain, intTime);
    
      for(current; current < 256; current+=15){  
      //... for each current step ...
        for(pwm; pwm < 256; pwm+=15){
        //... do measurements for each pwm step
          
          LP55231_lightGreen(pwm, current);
          TCS3472_getRawG(&data);
          voltage = LP55231_getVoltage(LP55231_CHANNEL_G); 
          temperatureLED = LP55231_getTemperature();

          String dataString = createString(pwm, current, 
                                           temperatureLED, data, 
                                           voltage, intTime, gain); 
          Serial.println(dataString);
          /*
          //create JSON string
          char dataString[800];
          createJSON(dataString, &current, &pwm, &temperatureLED, &data, 
                    &voltage, &intTime, &gain);  
          */
          //write string to .txt file
          writeToSD(dataString, intTime, gain);
          if(pwm == 255)
            writeToSD('\0', intTime, gain);
          //Serial.print("Finished PWM: "); Serial.println(pwm);
        }
        pwm = 0;
        //Serial.print("Finished current: "); Serial.println(current);    
      }
      current = 0;  
      //Serial.println("Finished gain: "); Serial.println(gain);  
    }
    gain = 0;
    //Serial.println("Finished int.Time"); Serial.println(intTime);
    switch(intTime){
      case TCS3472_INT_TIME_63:
        intTime = TCS3472_INT_TIME_125;
        break; 
      case TCS3472_INT_TIME_125:
        intTime = TCS3472_INT_TIME_250;
        break; 
      case TCS3472_INT_TIME_250:
        intTime = TCS3472_INT_TIME_500;
        break; 
      case TCS3472_INT_TIME_500:
        stopLoop = true;
        break; 
    } 
  }while(!stopLoop);
     
  TCS3472_disable();
  TCA9548A_disable();
  LP55231_ledsOff();
  LP55231_disable();

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second

}
//-------------------------------------------------------------------------------------------------------
//*****************************************************************************************************
//*                 SD-Card Functions                                                                 *
//*****************************************************************************************************

void writeToSD(char *jsonString, uint8_t intTime, uint8_t gain){
//open .txt File, writes the jsonString and close .txt
  //open measurementFile
  String strIntTime;
  switch(intTime){
    case TCS3472_INT_TIME_63:
      strIntTime = "63";
      break;  
    case TCS3472_INT_TIME_125:
      strIntTime = "125";
      break;  
    case TCS3472_INT_TIME_250:
      strIntTime = "250";
      break;  
    case TCS3472_INT_TIME_500:
      strIntTime = "500";
      break;  
  }
  
  String strGain;
  switch(gain){
    case TCS3472_GAIN_1X:
      strGain = "x1";
      break;  
    case TCS3472_GAIN_4X:
      strGain = "x4";
      break;  
    case TCS3472_GAIN_16X:
      strGain = "x16";
      break;  
    case TCS3472_GAIN_60X:
      strGain = "x60";
      break;  
  }
  
  String path = strIntTime + strGain + ".txt";
  //Serial.print("Write File to SD Card: "); Serial.println(path);

  File measurementFile = SD.open(path, FILE_WRITE);

  //if file opened successfully, write jsonString
  if(measurementFile){
    measurementFile.println(jsonString);
    measurementFile.close();
  }
  else {
    //if measurementFile did not open, print error
    Serial.println("SD Card Error opening .txt File");
  }
}

void writeToSD(String dataString, uint8_t intTime, uint8_t gain){
//open .txt File, writes the jsonString and close .txt
  //open measurementFile
  String strIntTime;
  switch(intTime){
    case TCS3472_INT_TIME_63:
      strIntTime = "63";
      break;  
    case TCS3472_INT_TIME_125:
      strIntTime = "125";
      break;  
    case TCS3472_INT_TIME_250:
      strIntTime = "250";
      break;  
    case TCS3472_INT_TIME_500:
      strIntTime = "500";
      break;  
  }
  
  String strGain;
  switch(gain){
    case TCS3472_GAIN_1X:
      strGain = "x1";
      break;  
    case TCS3472_GAIN_4X:
      strGain = "x4";
      break;  
    case TCS3472_GAIN_16X:
      strGain = "x16";
      break;  
    case TCS3472_GAIN_60X:
      strGain = "x60";
      break;  
  }
  
  String path = strIntTime + strGain + ".txt";
  //Serial.print("Write File to SD Card: "); Serial.println(path);

  File measurementFile = SD.open(path, FILE_WRITE);

  //if file opened successfully, write jsonString
  if(measurementFile){
    measurementFile.print(dataString);
    measurementFile.close();
  }
  else {
    //if measurementFile did not open, print error
    Serial.println("SD Card Error opening .txt File");
  }
}

//*****************************************************************************************************
//*                 JSON Functions                                                                    *
//*****************************************************************************************************
String createString(uint16_t pwm, uint16_t current, 
                int8_t temperatureLED, uint16_t value, 
                float LP55231voltage, uint8_t intTime, 
                uint8_t gain){
  
  String response = String(pwm) + ' ' + String (current) + ' ' +
                    String(temperatureLED) + ' ' + String(value) + ' ' +
                    String(LP55231voltage) + ' ' + String(intTime) + ' ' +
                    String(gain) + '\n';
                                     
  return response;                    
}

void createJSON(char *jsonString, uint16_t *current, uint16_t *pwm, 
                int8_t *temperatureLED, uint16_t *value, 
                float *LP55231voltage, uint8_t *intTime, 
                uint8_t *gain){
  
  const size_t capacity = JSON_OBJECT_SIZE(7) + 150;
  DynamicJsonDocument doc(capacity);

  doc["PWM"] = *pwm;
  doc["Current"] = *current;
  doc["TemperatureLED"] = *temperatureLED;
  doc["R"] = *value;
  doc["VoltageR"] = *LP55231voltage;
  doc["TCS3472intTime"] = *intTime;  
  doc["TCS3472gain"] = *gain;  

  if(!serializeJson(doc, jsonString, 800)) Serial.println("Error creating json string");
}

//*****************************************************************************************************
//*                 VEML6070 Functions                                                                *
//*****************************************************************************************************

bool VEML6070_clearAck(){
    return i2cRead(VEML6070_ADDR_ARA, 1);
}

void VEML6070_enable(){
//set inital values for reserved bits and set SD bit to 0
  _VEML6070_command_register = 0x02;
   
  VEML6070_clearAck();
  i2cWrite(VEML6070_I2C_L, _VEML6070_command_register);
}

void VEML6070_disable(){
//sets SD bit to 1
  _VEML6070_command_register |= VEML6070_SD_DISABLE;
  i2cWrite(VEML6070_I2C_L, _VEML6070_command_register);
}

uint16_t VEML6070_getRawData(uint8_t INTEGRATION_TIME){
//returns UV value
  //set integration time  
  _VEML6070_command_register |= INTEGRATION_TIME;
  i2cWrite(VEML6070_I2C_L, _VEML6070_command_register);
    
  //wait for one integration period
  VEML6070_wait(INTEGRATION_TIME);
  delay(250);

  //read values and calculate result
  uint16_t result = i2cRead(VEML6070_I2C_H, 1);
 
  result <<= 8;
  result |= i2cRead(VEML6070_I2C_L, 1);

  return result;
}

void VEML6070_wait(uint8_t INTEGRATION_TIME){
//wait for integration
  if(INTEGRATION_TIME == VEML6070_INT_HALF)
    delay(63);  //minimal integration time for 300k resistor
    //delay(126); //minimal integration time for 600k resistor
  else if(INTEGRATION_TIME == VEML6070_INT_1)
    delay(125);  //minimal integration time for 300k resistor
    //delay(250); //minimal integration time for 600k resistor 
  else if(INTEGRATION_TIME == VEML6070_INT_2)
     delay(250);  //minimal integration time for 300k resistor
    //delay(500); //minimal integration time for 600k resistor 
  else if(INTEGRATION_TIME == VEML6070_INT_4)
     delay(500);  //minimal integration time for 300k resistor
    //delay(1000); //minimal integration time for 600k resistor     
}

void VEML6070_getUV(uint8_t INTEGRATION_TIME, uint16_t *uv){
//enable VEML6070, read data and disable
  VEML6070_enable();
  *uv = VEML6070_getRawData(INTEGRATION_TIME);
  VEML6070_disable();
}

//*****************************************************************************************************
//*                 TSL2572 Functions                                                                 *
//*****************************************************************************************************

void TSL2572_enable(uint8_t TSL2572_GAIN_LEVEL, uint8_t integrationTime){
//power on and enable ALS, set wait long and ALS gain level (1/6) to 0
  //set wait long and ALS gain level (1/6) to 0
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_CONFIG, 0x00);
  
  //set gain level
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_GAIN, TSL2572_GAIN_LEVEL);
    
  //check if gain 1/6 is enabled and set if true
  if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_1X))
    i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_CONFIG, 0x04);
  else if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_8X))
    i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_CONFIG, 0x04);
  else if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_16X)){
    Serial.println("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_16x");
    return;  
  }
  else if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_120X)){
    Serial.println("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_120x");
    return;   
  }

  //set integration time
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_TIME, integrationTime); 
  //power on and enable ALS
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ENABLE, 0x03);
  //wait for ALS initialisation
  delay(10);
}

void TSL2572_disable(){
//power off and disable ALS
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ENABLE, 0x00);
}

uint16_t TSL2572_getRawData(uint8_t TSL2572_GAIN_LEVEL){
//get IR value for given gain level and integration time
  //wait for one integration period
  delay((uint16_t)TSL2572_getIntegrationTime());
  delay(250);
  
  //read IR measurements from data registers
  Wire.beginTransmission(TSL2572_I2C);
  Wire.write(TSL2572_AUTO_INCREMENT | TSL2572_DATA);
  Wire.endTransmission();
  Wire.requestFrom(TSL2572_I2C, 4);
  uint8_t data[4];
  for(uint8_t i = 0; i < 4; i++)
    data[i] = Wire.read();

  //calculate c0, c1 from data
  uint16_t c0 = 0, c1 = 0;  
  c0 = data[1]<<8 | data[0];
  c1 = data[3]<<8 | data[2];
  
  return max(c0, c1);
}

float TSL2572_getIntegrationTime(){
  uint8_t regValue = 0;
  regValue = i2cRead(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_TIME, 1);

  float integrationTime = 0;
  integrationTime = (256 - regValue) * 2.73;

  return integrationTime;
}

void TSL2572_getIR(uint8_t TSL2572_GAIN_LEVEL, uint8_t integrationTime, uint16_t *ir){
//initilialise TSL2572, enables PON and ALS, reads data and disables PON and ALS
  TSL2572_enable(TSL2572_GAIN_LEVEL, integrationTime);
  *ir = TSL2572_getRawData(TSL2572_GAIN_LEVEL);
  TSL2572_disable();
}

//*****************************************************************************************************
//*                 TCS3472 Functions                                                                 *
//*****************************************************************************************************

void TCS3472_enable(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime){
//set gain and integration time, power on and enable RGBC, set wait long to 0   
  //set PON and AEN 1
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_ENABLE, 0x03);
  //wait for RGBC initialisation
  delay(100);

  //set wait long to 0
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_CONFIG, 0x00);
  //set gain level
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_RGBC_GAIN, TCS3472_GAIN_LEVEL);
  //set integration time
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_RGBC_TIME, integrationTime);
}

void TCS3472_disable(){
//set PON and AEN to 0
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_ENABLE, 0x00);
}

void TCS3472_getRawR(uint16_t *r){
//get raw r value for given gain level and integration time
  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read r value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_RDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write r value to pointer
  *r = data[1]<<8 | data[0];
}

void TCS3472_getRawG(uint16_t *g){
//get raw g value for given gain level and integration time
  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read g value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_GDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write g value to pointer
  *g = data[1]<<8 | data[0];
}

void TCS3472_getRawB(uint16_t *b){
//get raw b value for given gain level and integration time
  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read b value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_BDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write b value to pointer
  *b = data[1]<<8 | data[0];
}

void TCS3472_getRawC(uint16_t *c){
//get raw c value for given gain level and integration time
  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read c value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_CDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write c value to pointer
  *c = data[1]<<8 | data[0];
}

void TCS3472_getRawRGBC(uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *c){
//get r, g, b, c values for given gain level and integration time
  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read r, g, b, c values from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_CDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 8);
  uint8_t data[8];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();
  
  //calculate r, g, b, c from data
  *c = data[1]<<8 | data[0];
  *r = data[3]<<8 | data[2];
  *g = data[5]<<8 | data[4];
  *b = data[7]<<8 | data[6];
}

uint16_t TCS3472_getIntegrationTime(){
//calculate integrtion time in ms from register value  
  uint8_t regValue = i2cRead(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_RGBC_TIME, 1);
  uint16_t integrationTime = (256 - regValue) * 2.4;
  return integrationTime;
}

void TCS3472_getR(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *r){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawR(r);
  TCS3472_disable();
}

void TCS3472_getG(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *g){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawG(g);
  TCS3472_disable();
}

void TCS3472_getB(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *b){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawB(b);
  TCS3472_disable();
}

void TCS3472_getC(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *c){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawC(c);
  TCS3472_disable();
}

void TCS3472_getRGBC(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *c){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawRGBC(r, g, b, c);
  TCS3472_disable();
}

//*****************************************************************************************************
//*                 LP55231 Functions                                                                 *
//*****************************************************************************************************

void LP55231_enable(){
//set enable bit of LP55231
  i2cWrite(LP55231_I2C, LP55231_REG_CNTRL1, 0x40);
  //enable internal clock & charge pump & write auto increment 
  i2cWrite(LP55231_I2C, LP55231_REG_MISC, 0x53);
}

void LP55231_disable(){
//set enable bit of LP55231 to 0
  i2cWrite(LP55231_I2C, LP55231_REG_CNTRL1, 0x00);
}

void LP55231_reset(){
//set reset register of LP55231
  i2cWrite(LP55231_I2C, LP55231_REG_RESET, 0xff);
}

void LP55231_lightRed(uint8_t pwm, uint8_t current){
//light red LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D9_PWM, pwm);
  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D9_I_CTL, current);
}

void LP55231_lightGreen(uint8_t pwm, uint8_t current){
//light green LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D5_PWM, pwm);
  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D5_I_CTL, current);
}

void LP55231_lightBlue(uint8_t pwm, uint8_t current){
//light blue LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D6_PWM, pwm);
  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D6_I_CTL, current);
}

void LP55231_lightUV(uint8_t pwm, uint8_t current){
//light UV LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D8_PWM, pwm);
  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D8_I_CTL, current);
}

void LP55231_lightIR(uint8_t pwm, uint8_t current){
//light IR LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D7_PWM, pwm);
  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D7_I_CTL, current);
}

void LP55231_ledsOff(){
//set current and PWM duty cycle of all LEDs to 0
  i2cWrite(LP55231_I2C, LP55231_REG_D9_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D9_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D5_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D5_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D6_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D6_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D8_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D8_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D7_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D7_PWM, 0);  
}

int8_t LP55231_getTemperature(){
  uint8_t active; //indicates if the temperature measurement is active
  int8_t temperature;

  //command for reading the sensor once
  i2cWrite(LP55231_I2C, LP55231_REG_TEMP_CTL, 0x04);
  //wait until measurement is finished
  do{
    active = i2cRead(LP55231_I2C, LP55231_REG_TEMP_CTL, 1);  
  }while(active & 0x80);

  temperature = (int8_t)i2cRead(LP55231_I2C, LP55231_REG_TEMP_DATA, 1);
  return temperature;
}

float LP55231_getVoltage(uint8_t LP55231_CHANNEL){
  uint8_t data;
  float voltage;

  data = LP55231_readADCinternal(LP55231_CHANNEL & 0x0f);
  voltage = (data * 0.03) - 1.478;
  return voltage;
}

uint8_t LP55231_readADCinternal(uint8_t LP55231_CHANNEL){
  i2cWrite(LP55231_I2C, LP55231_REG_VOLTAGE_CTL, 0x80 | (LP55231_CHANNEL & 0x1f));
  //wait for writing register
  delay(10);

  return i2cRead(LP55231_I2C, LP55231_REG_VOLTAGE_DATA, 1);
}

//*****************************************************************************************************
//*                 I2C-Switch Functions                                                              *
//*****************************************************************************************************

void TCA9548A_enableVEML6070(){
//enables Channel 0 of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x01);
}

void TCA9548A_enableTCS3472(){
//enables Channel 1 of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x02);
}

void TCA9548A_enableTSL2572(){
//enables Channel 2 of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x04);
}

void TCA9548A_disable(){
//disables Channels of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x00);
}

//*****************************************************************************************************
//*                 I2C Read/Write Functions                                                          *
//*****************************************************************************************************

void i2cWrite(uint8_t adress, uint8_t reg, int value){
//Write data on i2c-device
  Wire.beginTransmission(adress);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();

  //wait 10 ms for Transmission
  delay(10);
}

void i2cWrite(uint8_t adress, int value){
//Write data on i2c-device
  Wire.beginTransmission(adress);
  Wire.write(value);
  Wire.endTransmission();
  
  //wait 10 ms for Transmission
  delay(10);
}

int i2cRead(uint8_t adress, uint8_t reg, int bytes){
//read data from i2c-Device
  int result = 0;
  
  //Pointer on register
  Wire.beginTransmission(adress);
  Wire.write(reg);
  Wire.endTransmission();
  
  //wait 10 ms for Transmission
  delay(10);

  //read data until recieving buffer is empty
  Wire.requestFrom(adress, bytes);
  while(Wire.available()){
    uint8_t i;
    uint8_t buff = 0;
    for (i = 0; i < bytes; i++){
      buff = Wire.read();
      result = (result << 8) | buff;
    }
  }
  return result;
}

int i2cRead(uint8_t adress, int bytes){
//read data from i2c-Device
  uint16_t result = 0;

  //read data until recieving buffer is empty
  Wire.requestFrom(adress, bytes);

  //read every byte and calculate result
  while(Wire.available()){
    uint8_t i;
    uint8_t buff = 0;
    for (i = 0; i < bytes; i++){
      buff = Wire.read();
      result = (result << 8) | buff;
    }
  } 
  return result;
}
