#include "deepseagui.h"
#include "ui_deepseagui.h"

#include <QSerialPortInfo>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QFileDialog>
#include <QtCharts>
#include <QChartView>

DeepSeaGUI::DeepSeaGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DeepSeaGUI)
{
    ui->setupUi(this);

    //initialize Combobox
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        ui->m_serialPortComboBox->addItem(info.portName());
    ui->m_statusTextEdit->clear();

    ui->m_TSL2572GainComboBox->addItem("1x");
    ui->m_TSL2572GainComboBox->addItem("8x");
    ui->m_TSL2572GainComboBox->addItem("16x");
    ui->m_TSL2572GainComboBox->addItem("120x");

    ui->m_TCS3472RedGainComboBox->addItem("1x");
    ui->m_TCS3472RedGainComboBox->addItem("4x");
    ui->m_TCS3472RedGainComboBox->addItem("16x");
    ui->m_TCS3472RedGainComboBox->addItem("60x");

    ui->m_TCS3472GreenGainComboBox->addItem("1x");
    ui->m_TCS3472GreenGainComboBox->addItem("4x");
    ui->m_TCS3472GreenGainComboBox->addItem("16x");
    ui->m_TCS3472GreenGainComboBox->addItem("60x");

    ui->m_TCS3472BlueGainComboBox->addItem("1x");
    ui->m_TCS3472BlueGainComboBox->addItem("4x");
    ui->m_TCS3472BlueGainComboBox->addItem("16x");
    ui->m_TCS3472BlueGainComboBox->addItem("60x");

    ui->m_TCS3472ClearGainComboBox->addItem("1x");
    ui->m_TCS3472ClearGainComboBox->addItem("4x");
    ui->m_TCS3472ClearGainComboBox->addItem("16x");
    ui->m_TCS3472ClearGainComboBox->addItem("60x");

    ui->m_VEML6070IntegrationtimeComboBox->addItem("0.5x");
    ui->m_VEML6070IntegrationtimeComboBox->addItem("1x");
    ui->m_VEML6070IntegrationtimeComboBox->addItem("2x");
    ui->m_VEML6070IntegrationtimeComboBox->addItem("4x");

    ui->m_pressureADCPrecisionComboBox->addItem("256");
    ui->m_pressureADCPrecisionComboBox->addItem("512");
    ui->m_pressureADCPrecisionComboBox->addItem("1024");
    ui->m_pressureADCPrecisionComboBox->addItem("2048");
    ui->m_pressureADCPrecisionComboBox->addItem("4096");

    ui->m_temperatureADCPrecisionComboBox->addItem("256");
    ui->m_temperatureADCPrecisionComboBox->addItem("512");
    ui->m_temperatureADCPrecisionComboBox->addItem("1024");
    ui->m_temperatureADCPrecisionComboBox->addItem("2048");
    ui->m_temperatureADCPrecisionComboBox->addItem("4096");

    ui->m_measurementMethodComboBox->addItem("Single Measurement");
    ui->m_measurementMethodComboBox->addItem("Continuous Measurement");

    if (ui->m_measurementMethodComboBox->currentText() ==
            "Single Measurement"){
        m_continuousMeasurement = false;
        ui->m_sampleRateSpinBox->setEnabled(false);
        ui->m_numberSamplesSpinBox->setEnabled(false);
    }

    ui->m_TSL2572TimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TSL2572IntegrationtimeSpinBox->value()*2.73));
    ui->m_TCS3472RedTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472RedIntegrationtimeSpinBox->value()*2.4));
    ui->m_TCS3472GreenTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472GreenIntegrationtimeSpinBox->value()*2.4));
    ui->m_TCS3472BlueTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472BlueIntegrationtimeSpinBox->value()*2.4));
    ui->m_TCS3472ClearTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472ClearIntegrationtimeSpinBox->value()*2.4));
    ui->m_resultingSampleTimeLabel->setText(QString("%1 s")
    .arg(ui->m_sampleRateSpinBox->value() * (ui->m_numberSamplesSpinBox)->value()));

    //create charts
    m_onlineChart = createChart();
    m_onlineChartView = new QChartView(m_onlineChart);
    ui->m_onlineChartVerticalLayout->addWidget(m_onlineChartView);
    //ui->m_resultsTabWidget->removeTab(0);
    //ui->m_resultsTabWidget->insertTab(0, onlineChartView, "Online Chart");
    ui->m_resultsTabWidget->setCurrentIndex(0);
    m_onlineChartView->setRenderHint(QPainter::Antialiasing, true);

    //connect signals to slots
    connect(&m_thread, &MasterThread::response, this, &DeepSeaGUI::writeResponse);
    connect(&m_thread, &MasterThread::error, this, &DeepSeaGUI::processError);
    connect(&m_thread, &MasterThread::timeout, this, &DeepSeaGUI::processTimeout);
}

DeepSeaGUI::~DeepSeaGUI()
{
    delete ui;
}


void DeepSeaGUI::on_m_serialPortRefreshButton_clicked()
{
    ui->m_serialPortComboBox->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        ui->m_serialPortComboBox->addItem(info.portName());

    ui->m_statusTextEdit->clear();
}

void DeepSeaGUI::on_m_TSL2572IntegrationtimeSpinBox_valueChanged()
{
    ui->m_TSL2572TimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TSL2572IntegrationtimeSpinBox->value()*2.73));
}

void DeepSeaGUI::on_m_TCS3472RedIntegrationtimeSpinBox_valueChanged()
{
    ui->m_TCS3472RedTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472RedIntegrationtimeSpinBox->value()*2.4));
}

void DeepSeaGUI::on_m_TCS3472GreenIntegrationtimeSpinBox_valueChanged()
{
    ui->m_TCS3472GreenTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472GreenIntegrationtimeSpinBox->value()*2.4));
}

void DeepSeaGUI::on_m_TCS3472BlueIntegrationtimeSpinBox_valueChanged()
{
    ui->m_TCS3472BlueTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472BlueIntegrationtimeSpinBox->value()*2.4));
}

void DeepSeaGUI::on_m_TCS3472ClearIntegrationtimeSpinBox_valueChanged()
{
    ui->m_TCS3472ClearTimeResultLabel->setText(QString("%1 ms")
    .arg(ui->m_TCS3472ClearIntegrationtimeSpinBox->value()*2.4));
}

void DeepSeaGUI::on_m_measurementMethodComboBox_currentTextChanged()
{
    if (ui->m_measurementMethodComboBox->currentText() ==
            "Single Measurement"){
        m_continuousMeasurement = false;
        ui->m_sampleRateSpinBox->
                setValue(ui->m_sampleRateSpinBox->minimum());
        ui->m_sampleRateSpinBox->setEnabled(false);
        ui->m_numberSamplesSpinBox->
                setValue(ui->m_numberSamplesSpinBox->minimum());
        ui->m_numberSamplesSpinBox->setEnabled(false);
    } else if (ui->m_measurementMethodComboBox->currentText() ==
               "Continuous Measurement"){
        m_continuousMeasurement = true;
        ui->m_sampleRateSpinBox->setEnabled(true);
        ui->m_numberSamplesSpinBox->setEnabled(true);
    }
}

void DeepSeaGUI::on_m_sampleRateSpinBox_valueChanged()
{
    ui->m_resultingSampleTimeLabel->setText(QString("%1 s")
    .arg(ui->m_numberSamplesSpinBox->value() * ui->m_sampleRateSpinBox->value()));
}

void DeepSeaGUI::on_m_numberSamplesSpinBox_valueChanged()
{
    ui->m_resultingSampleTimeLabel->setText(QString("%1 s")
    .arg(ui->m_numberSamplesSpinBox->value() * ui->m_sampleRateSpinBox->value()));
}

void DeepSeaGUI::on_m_VEML6070IntegrationtimeComboBox_activated()
{
    //for 300kOhm Resistor
    if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="0.5x")
        ui->m_VEML6070TimeResultLabel->setText("62.5 ms");
    else if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="1x")
        ui->m_VEML6070TimeResultLabel->setText("125 ms");
    else if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="2x")
        ui->m_VEML6070TimeResultLabel->setText("250 ms");
    else if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="4x")
        ui->m_VEML6070TimeResultLabel->setText("500 ms");

    //for 600kOhm Resistor
    /*
    if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="0.5x")
        ui->m_VEML6070TimeResultLabel->setText("125 ms");
    else if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="1x")
        ui->m_VEML6070TimeResultLabel->setText("250 ms");
    else if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="2x")
        ui->m_VEML6070TimeResultLabel->setText("500 ms");
    else if(ui->m_VEML6070IntegrationtimeComboBox->currentText()=="4x")
        ui->m_VEML6070TimeResultLabel->setText("1000 ms");
    */
}

void DeepSeaGUI::on_m_serialPortComboBox_currentTextChanged()
{
    //"" an port senden; dadurch funktioniert die Kommunikation beim ersten Klick auf den Button
    m_thread.transaction(ui->m_serialPortComboBox->currentText(), 2000, "", 1);
    ui->m_statusTextEdit->clear();
}

void DeepSeaGUI::on_m_chooseFileButton_clicked()
{

    m_currentFilePath = QFileDialog::getOpenFileName(this,
                          "Choose File for Data", "/Users/chris/Desktop", //QDir::currentPath(),
                          "Text files (*.txt)");

    ui->m_currentFileTextEdit->setText(m_currentFilePath);
}

void DeepSeaGUI::setControlsEnabled(bool enable)
{
    //enable/disable control widgets
    /*
    ui->m_PWMUVSpinBox->setEnabled(enable);
    ui->m_currentUVSpinBox->setEnabled(enable);
    ui->m_PWMIRSpinBox->setEnabled(enable);
    ui->m_currentIRSpinBox->setEnabled(enable);
    ui->m_PWMRSpinBox->setEnabled(enable);
    ui->m_currentRSpinBox->setEnabled(enable);
    ui->m_PWMGSpinBox->setEnabled(enable);
    ui->m_currentGSpinBox->setEnabled(enable);
    ui->m_PWMBSpinBox->setEnabled(enable);
    ui->m_currentBSpinBox->setEnabled(enable);
    ui->m_TSL2572GainComboBox->setEnabled(enable);
    ui->m_TSL2572IntegrationtimeSpinBox->setEnabled(enable);
    ui->m_TCS3472RedGainComboBox->setEnabled(enable);
    ui->m_TCS3472RedIntegrationtimeSpinBox->setEnabled(enable);
    ui->m_TCS3472GreenGainComboBox->setEnabled(enable);
    ui->m_TCS3472GreenIntegrationtimeSpinBox->setEnabled(enable);
    ui->m_TCS3472BlueGainComboBox->setEnabled(enable);
    ui->m_TCS3472BlueIntegrationtimeSpinBox->setEnabled(enable);
    ui->m_TCS3472ClearGainComboBox->setEnabled(enable);
    ui->m_TCS3472ClearIntegrationtimeSpinBox->setEnabled(enable);
    ui->m_VEML6070IntegrationtimeComboBox->setEnabled(enable);
    ui->m_pressureADCPrecisionComboBox->setEnabled(enable);
    ui->m_temperatureADCPrecisionComboBox->setEnabled(enable);
    */
    ui->m_serialPortComboBox->setEnabled(enable);
    ui->m_serialPortRefreshButton->setEnabled(enable);
    ui->m_chooseFileButton->setEnabled(enable);
    ui->m_startMeasurementButton->setEnabled(enable);
    ui->m_measurementMethodComboBox->setEnabled(enable);

    /*
    if (ui->m_measurementMethodComboBox->currentText() ==
            "Single Measurement"){
        m_continuousMeasurement = false;
        ui->m_sampleRateSpinBox->setEnabled(false);
        ui->m_numberSamplesSpinBox->setEnabled(false);
    } else{
        ui->m_numberSamplesSpinBox->setEnabled(enable);
        ui->m_sampleRateSpinBox->setEnabled(enable);
      }
    */
}

void DeepSeaGUI::on_m_showIRCheckBox_stateChanged()
{
    visualizeSeries(ui->m_showIRCheckBox->isChecked(),
                    m_seriesIR, m_axisYir);
}

void DeepSeaGUI::on_m_showUVCheckBox_stateChanged()
{
    visualizeSeries(ui->m_showUVCheckBox->isChecked(),
                    m_seriesUV, m_axisYuv);
}

void DeepSeaGUI::on_m_showPressureCheckBox_stateChanged()
{
    visualizeSeries(ui->m_showPressureCheckBox->isChecked(),
                    m_seriesPressure, m_axisYpressure);
}

void DeepSeaGUI::on_m_showTemperatureCheckBox_stateChanged()
{
    visualizeSeries(ui->m_showTemperatureCheckBox->isChecked(),
                    m_seriesTemperature, m_axisYtemperature);
}

void DeepSeaGUI::on_m_showRedCheckBox_stateChanged()
{
    visualizeSeriesRGBC(ui->m_showRedCheckBox->isChecked(),
                        m_seriesRed, m_axisYrgbc);
}

void DeepSeaGUI::on_m_showGreenCheckBox_stateChanged()
{
    visualizeSeriesRGBC(ui->m_showGreenCheckBox->isChecked(),
                        m_seriesGreen, m_axisYrgbc);
}

void DeepSeaGUI::on_m_showBlueCheckBox_stateChanged()
{
    visualizeSeriesRGBC(ui->m_showBlueCheckBox->isChecked(),
                        m_seriesBlue, m_axisYrgbc);
}

void DeepSeaGUI::on_m_showClearCheckBox_stateChanged()
{
    visualizeSeriesRGBC(ui->m_showClearCheckBox->isChecked(),
                        m_seriesClear, m_axisYrgbc);
}

void DeepSeaGUI::on_m_clearDataButton_clicked()
{
    if(ui->m_currentFileTextEdit->toPlainText() == "")
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Clear Data",
                "Are you sure that you want to clear the data from the chart?",
                QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::No)
            return;
    }

    m_seriesRed->clear();
    m_seriesGreen->clear();
    m_seriesBlue->clear();
    m_seriesClear->clear();
    m_seriesIR->clear();
    m_seriesUV->clear();
    m_seriesPressure->clear();
    m_seriesTemperature->clear();

    m_axisYir->setRange(0,0);
    m_axisYuv->setRange(0,0);
    m_axisYrgbc->setRange(0,0);
    m_axisYpressure->setRange(0,0);
    m_axisYtemperature->setRange(0,0);
}

void DeepSeaGUI::on_m_saveChartButton_clicked()
{
    QString path = QFileDialog::getSaveFileName(this, tr("Save File"),
                       m_currentFilePath.remove(".txt"), tr("Images (*.png)"));

    QPixmap pixmap(m_onlineChartView->size());
    pixmap = m_onlineChartView->grab();
    pixmap.save(path, "PNG");
}

void DeepSeaGUI::on_m_startMeasurementButton_clicked()
{
    if(ui->m_currentFileTextEdit->toPlainText() == "")
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Start Measurement",
                "No file for writing raw data choosen. Continue anyway?",
                QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::No)
            return;
    }

    setControlsEnabled(false);
    m_responseCount = 0;
    ui->m_statusTextEdit->append(tr("Status: Running Request #%1, "
                                    "connected to port %2").
                               arg(++m_transactionCount).
                               arg(ui->m_serialPortComboBox->currentText()));
    ui->m_statusTextEdit->ensureCursorVisible();

    createCommand();

    //response time = sample rate + 2s
    m_thread.transaction(ui->m_serialPortComboBox->currentText(),
                         ui->m_sampleRateSpinBox->value()*1000 + 6000,
                         m_measurementParameters,
                         ui->m_numberSamplesSpinBox->value());


    QDateTime currentTime = QDateTime::currentDateTimeUtc();
    ui->m_responseTextEdit->append(QString("Traffic, transaction #%1:"
                                      "\n%2; Request: %3\n")
                                 .arg(m_transactionCount)
                                 .arg(currentTime.toUTC().toString())
                                 .arg(m_measurementParameters));
    ui->m_responseTextEdit->ensureCursorVisible();

    m_measurementParameters.clear();
}

void DeepSeaGUI::writeResponse(const QString &response)
{
    m_responseCount++;
    if (m_responseCount == ui->m_numberSamplesSpinBox->value())
        setControlsEnabled(true);

    QDateTime currentTime = QDateTime::currentDateTimeUtc();
    ui->m_responseTextEdit->append(QString("%1: Response #%2: %3")
                                   .arg(currentTime.toUTC().toString())
                                   .arg(m_responseCount)
                                   .arg(response));
    ui->m_responseTextEdit->ensureCursorVisible();

    ui->m_statusTextEdit->append(tr("Recieved Response #%1").
                                 arg(m_responseCount));
    ui->m_statusTextEdit->ensureCursorVisible();

    //parse response string
    parseResponse(response);
    updateOnlineChart();

    //create .txt file and write data
    QFile dataFile(m_currentFilePath);
    //dataFile.close();
    if(!dataFile.open(QIODevice::WriteOnly | QIODevice::Append)){
        ui->m_statusTextEdit->append("Status: failed to write data to .txt file.");
        ui->m_statusTextEdit->ensureCursorVisible();
        return;
    } else {
    QTextStream text(&dataFile);
    text << currentTime.toString() << ": " << response;
    dataFile.close();
    }
}

void DeepSeaGUI::processError(const QString &s)
{
    setControlsEnabled(true);
    ui->m_statusTextEdit->append(tr("Status: Not running, %1.\nNo traffic.").arg(s));
    ui->m_statusTextEdit->ensureCursorVisible();
}

void DeepSeaGUI::processTimeout(const QString &s)
{
    setControlsEnabled(true);
    ui->m_statusTextEdit->append(tr("Status: Running, %1.\nNo traffic.").arg(s));
    ui->m_statusTextEdit->ensureCursorVisible();
}

void DeepSeaGUI::createCommand()
{
    m_measurementParameters.append(ui->m_PWMUVSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_currentUVSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_PWMIRSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append((ui->m_currentIRSpinBox->text()));
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_PWMRSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_currentRSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_PWMGSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_currentGSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_PWMBSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_currentBSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TSL2572IntegrationtimeSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TSL2572GainComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472RedIntegrationtimeSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472GreenIntegrationtimeSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472BlueIntegrationtimeSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472ClearIntegrationtimeSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472RedGainComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472GreenGainComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472BlueGainComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_TCS3472ClearGainComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_VEML6070IntegrationtimeComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_pressureADCPrecisionComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_temperatureADCPrecisionComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_measurementMethodComboBox->currentText());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_numberSamplesSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append(ui->m_sampleRateSpinBox->text());
    m_measurementParameters.append(",");
    m_measurementParameters.append("/");
}

void DeepSeaGUI::parseResponse(const QString &response)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(response.toUtf8());
    m_redValue = jsonDoc.object().value("R").toInt();
    m_greenValue = jsonDoc.object().value("G").toInt();
    m_blueValue = jsonDoc.object().value("B").toInt();
    m_clearValue = jsonDoc.object().value("C").toInt();
    m_uvValue = jsonDoc.object().value("UV").toInt();
    m_irValue = jsonDoc.object().value("IR").toInt();
    m_pressureValue = jsonDoc.object().value("Pressure").toDouble();
    m_temperatureValue = jsonDoc.object().value("Temperature").toDouble();
    /*
    ui->m_statusTextEdit->append(jsonDoc.toJson(QJsonDocument::Compact));
    ui->m_statusTextEdit->append(tr("R: %1").arg(m_redValue));
    ui->m_statusTextEdit->append(tr("G: %1").arg(m_greenValue));
    ui->m_statusTextEdit->append(tr("B: %1").arg(m_blueValue));
    ui->m_statusTextEdit->append(tr("C: %1").arg(m_clearValue));
    */
}

QChart* DeepSeaGUI::createChart()
{
    QDateTime currentTime = QDateTime::currentDateTime();

    QChart *chart = new QChart();
    chart->legend()->setAlignment(Qt::AlignBottom);
    chart->legend()->show();

    //create date axis with default values
    m_axisX = new QDateTimeAxis;
    m_axisX->setTitleText("Date");
    m_axisX->setTickCount(7);
    m_axisX->setMin(currentTime);
    m_axisX->setMax(currentTime.addMSecs(5));
    m_axisX->setFormat("h:mm:ss"); //dd-MM-yyyy h:mm:ss
    //axisX->setRange(m_startTime, currentTime);

    //create RGBC axis with default values
    m_axisYrgbc = new QValueAxis;
    m_axisYrgbc->setTitleText("Red/Green/Blue/Clear (Counts)");
    m_axisYrgbc->setTickCount(10);
    m_axisYrgbc->setMin(0);
    m_axisYrgbc->setMax(0);
    m_axisYrgbc->setLabelFormat("%.1f");

    //create UV axis with default values
    m_axisYuv = new QValueAxis;
    m_axisYuv->setTitleText("UV (Counts)");
    m_axisYuv->setTickCount(10);
    m_axisYuv->setMin(0);
    m_axisYuv->setMax(0);
    m_axisYuv->setLabelFormat("%.1f");

    //create IR axis with default values
    m_axisYir = new QValueAxis;
    m_axisYir->setTitleText("IR (Counts)");
    m_axisYir->setTickCount(10);
    m_axisYir->setMin(0);
    m_axisYir->setMax(0);
    m_axisYir->setLabelFormat("%.1f");

    //create pressure axis with default values
    m_axisYpressure = new QValueAxis;
    m_axisYpressure->setTitleText("Pressure (mbar)");
    m_axisYpressure->setTickCount(10);
    m_axisYpressure->setMin(0);
    m_axisYpressure->setMax(0);
    m_axisYpressure->setLabelFormat("%.1f");

    //create temperature axis with default values
    m_axisYtemperature = new QValueAxis;
    m_axisYtemperature->setTitleText("Temperature (°C)");
    m_axisYtemperature->setTickCount(10);
    m_axisYtemperature->setMin(0);
    m_axisYtemperature->setMax(0);
    m_axisYtemperature->setLabelFormat("%.1f");

    //add axis to chart
    chart->addAxis(m_axisX, Qt::AlignBottom);
    chart->addAxis(m_axisYrgbc, Qt::AlignLeft);
    chart->addAxis(m_axisYuv, Qt::AlignLeft);
    chart->addAxis(m_axisYir, Qt::AlignLeft);
    chart->addAxis(m_axisYpressure, Qt::AlignRight);
    chart->addAxis(m_axisYtemperature, Qt::AlignRight);

    //add series to chart and attach axis to series
    m_seriesRed = new QLineSeries();
    m_seriesRed->setName("Red");
    m_seriesRed->setPointsVisible(true);
    chart->addSeries(m_seriesRed);
    m_seriesRed->attachAxis(m_axisX);
    m_seriesRed->attachAxis(m_axisYrgbc);

    m_seriesGreen = new QLineSeries();
    m_seriesGreen->setName("Green");
    m_seriesGreen->setPointsVisible(true);
    chart->addSeries(m_seriesGreen);
    m_seriesGreen->attachAxis(m_axisX);
    m_seriesGreen->attachAxis(m_axisYrgbc);

    m_seriesBlue = new QLineSeries();
    m_seriesBlue->setName("Blue");
    m_seriesBlue->setPointsVisible(true);
    chart->addSeries(m_seriesBlue);
    m_seriesBlue->attachAxis(m_axisX);
    m_seriesBlue->attachAxis(m_axisYrgbc);

    m_seriesClear = new QLineSeries();
    m_seriesClear->setName("Clear");
    m_seriesClear->setPointsVisible(true);
    chart->addSeries(m_seriesClear);
    m_seriesClear->attachAxis(m_axisX);
    m_seriesClear->attachAxis(m_axisYrgbc);

    m_seriesUV = new QLineSeries();
    m_seriesUV->setName("UV");
    m_seriesUV->setPointsVisible(true);
    chart->addSeries(m_seriesUV);
    m_seriesUV->attachAxis(m_axisX);
    m_seriesUV->attachAxis(m_axisYuv);

    m_seriesIR = new QLineSeries();
    m_seriesIR->setName("IR");
    m_seriesIR->setPointsVisible(true);
    chart->addSeries(m_seriesIR);
    m_seriesIR->attachAxis(m_axisX);
    m_seriesIR->attachAxis(m_axisYir);

    m_seriesPressure = new QLineSeries();
    m_seriesPressure->setName("Pressure");
    m_seriesPressure->setPointsVisible(true);
    chart->addSeries(m_seriesPressure);
    m_seriesPressure->attachAxis(m_axisX);
    m_seriesPressure->attachAxis(m_axisYpressure);

    m_seriesTemperature = new QLineSeries();
    m_seriesTemperature->setName("Temperature");
    m_seriesTemperature->setPointsVisible(true);
    chart->addSeries(m_seriesTemperature);
    m_seriesTemperature->attachAxis(m_axisX);
    m_seriesTemperature->attachAxis(m_axisYtemperature);

    QPen *pen = new QPen();
    pen->setWidth(2);
    pen->setBrush(Qt::red);
    m_seriesRed->setPen(*pen);
    pen->setBrush(Qt::darkGreen);
    m_seriesGreen->setPen(*pen);
    pen->setBrush(Qt::blue);
    m_seriesBlue->setPen(*pen);
    pen->setBrush(Qt::gray);
    m_seriesClear->setPen(*pen);
    pen->setBrush(Qt::darkBlue);
    m_seriesUV->setPen(*pen);
    pen->setBrush(Qt::magenta);
    m_seriesIR->setPen(*pen);
    pen->setBrush(Qt::darkCyan);
    m_seriesPressure->setPen(*pen);
    pen->setBrush(Qt::yellow);
    m_seriesTemperature->setPen(*pen);

    return chart;
}

void DeepSeaGUI::updateOnlineChart()
{
    QDateTime currentTime = QDateTime::currentDateTime();

    if(m_seriesIR->count() == 0)
        m_axisX->setRange(currentTime, currentTime.addSecs(5));
    else
        m_axisX->setMax(currentTime);
    //m_axisX->setMin(currentTime.addSecs(ui->m_sampleRateSpinBox->value()*(-10)));

    m_seriesRed->append(currentTime.toMSecsSinceEpoch(), m_redValue);
    m_seriesGreen->append(currentTime.toMSecsSinceEpoch(), m_greenValue);
    m_seriesBlue->append(currentTime.toMSecsSinceEpoch(), m_blueValue);
    m_seriesClear->append(currentTime.toMSecsSinceEpoch(), m_clearValue);
    m_seriesUV->append(currentTime.toMSecsSinceEpoch(), m_uvValue);
    m_seriesIR->append(currentTime.toMSecsSinceEpoch(), m_irValue);
    m_seriesPressure->append(currentTime.toMSecsSinceEpoch(), m_pressureValue);
    m_seriesTemperature->append(currentTime.toMSecsSinceEpoch(), m_temperatureValue);

    checkMax(m_axisYrgbc, m_redValue);
    checkMax(m_axisYrgbc, m_greenValue);
    checkMax(m_axisYrgbc, m_blueValue);
    checkMax(m_axisYrgbc, m_clearValue);
    checkMax(m_axisYuv, m_uvValue);
    checkMax(m_axisYir, m_irValue);
    checkMax(m_axisYpressure, m_pressureValue);
    checkMin(m_axisYpressure, m_pressureValue);
    checkMax(m_axisYtemperature, m_temperatureValue);
    checkMin(m_axisYtemperature, m_temperatureValue);
}

void DeepSeaGUI::checkMax(QValueAxis *axisY, int currentValue)
{
    if(currentValue > axisY->max())
    {
        axisY->setMax(currentValue + 10);
    }
}

void DeepSeaGUI::checkMax(QValueAxis *axisY, double currentValue)
{
    if(currentValue > axisY->max())
        axisY->setMax(currentValue + 10);
}

void DeepSeaGUI::checkMin(QValueAxis *axisY, double currentValue)
{
    if(currentValue < axisY->min())
        axisY->setMin(currentValue - 10);
}

void DeepSeaGUI::visualizeSeries(bool checked, QLineSeries *series, QValueAxis *axisY)
{
    if(checked)
    {
        series->setVisible(true);
        axisY->show();
    } else if(!checked)
    {
        series->setVisible(false);
        axisY->hide();
    }
}

void DeepSeaGUI::visualizeSeriesRGBC(bool checked, QLineSeries *series, QValueAxis *axisY)
{
    if(checked)
    {
        series->setVisible(true);
        axisY->show();
    } else if(!checked)
    {
        series->setVisible(false);

        //hide RGBC axis if all boxes are unchecked
        if(!ui->m_showRedCheckBox->isChecked() &&
           !ui->m_showGreenCheckBox->isChecked() &&
           !ui->m_showBlueCheckBox->isChecked() &&
           !ui->m_showClearCheckBox->isChecked())
            axisY->hide();
    }
}

void DeepSeaGUI::on_m_stopMeasurementButton_clicked()
{
    //m_thread.m_quit = true;
    /*
    m_thread.transaction(ui->m_serialPortComboBox->currentText(),
                         6000, "stop,", 1);
    */
}

void DeepSeaGUI::on_m_showPointsCheckBox_stateChanged()
{

    if(ui->m_pointsVisibleCheckBox->isChecked()){
        m_seriesIR->setPointsVisible(true);
        m_seriesUV->setPointsVisible(true);
        m_seriesRed->setPointsVisible(true);
        m_seriesGreen->setPointsVisible(true);
        m_seriesBlue->setPointsVisible(true);
        m_seriesClear->setPointsVisible(true);
        m_seriesPressure->setPointsVisible(true);
        m_seriesTemperature->setPointsVisible(true);
    }
    else {
        m_seriesIR->setPointsVisible(false);
        m_seriesUV->setPointsVisible(false);
        m_seriesRed->setPointsVisible(false);
        m_seriesGreen->setPointsVisible(false);
        m_seriesBlue->setPointsVisible(false);
        m_seriesClear->setPointsVisible(false);
        m_seriesPressure->setPointsVisible(false);
        m_seriesTemperature->setPointsVisible(false);
    }
    m_onlineChartView->repaint();

}

void DeepSeaGUI::on_m_legendVisibleCheckBox_stateChanged()
{
    if(ui->m_legendVisibleCheckBox->isChecked())
        m_onlineChart->legend()->show();
    else
        m_onlineChart->legend()->hide();
}

void DeepSeaGUI::on_actionLicense_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("Copyright © 2021 Christoph Strehse\n\n\
                   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\n\
                   This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\n\
                   You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.");
    msgBox.exec();
}
