#ifndef DEEPSEAGUI_H
#define DEEPSEAGUI_H

#include <QMainWindow>
#include <QtCharts>

#include "masterthread.h"

namespace Ui {
class DeepSeaGUI;
}

class DeepSeaGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit DeepSeaGUI(QWidget *parent = nullptr);
    ~DeepSeaGUI();

private slots:


    void on_m_serialPortRefreshButton_clicked();

    void on_m_TSL2572IntegrationtimeSpinBox_valueChanged();

    void on_m_TCS3472RedIntegrationtimeSpinBox_valueChanged();

    void on_m_VEML6070IntegrationtimeComboBox_activated();

    void on_m_startMeasurementButton_clicked();

    void on_m_serialPortComboBox_currentTextChanged();

    void on_m_sampleRateSpinBox_valueChanged();

    void on_m_numberSamplesSpinBox_valueChanged();

    void on_m_measurementMethodComboBox_currentTextChanged();

    void on_m_chooseFileButton_clicked();

    void on_m_TCS3472GreenIntegrationtimeSpinBox_valueChanged();

    void on_m_TCS3472BlueIntegrationtimeSpinBox_valueChanged();

    void on_m_TCS3472ClearIntegrationtimeSpinBox_valueChanged();

    void on_m_showIRCheckBox_stateChanged();

    void on_m_showUVCheckBox_stateChanged();

    void on_m_showPressureCheckBox_stateChanged();

    void on_m_showTemperatureCheckBox_stateChanged();

    void on_m_showRedCheckBox_stateChanged();

    void on_m_showGreenCheckBox_stateChanged();

    void on_m_showBlueCheckBox_stateChanged();

    void on_m_showClearCheckBox_stateChanged();

    void on_m_showPointsCheckBox_stateChanged();

    void on_m_legendVisibleCheckBox_stateChanged();

    void on_m_clearDataButton_clicked();

    void on_m_saveChartButton_clicked();

    void on_m_stopMeasurementButton_clicked();

    void writeResponse(const QString &response);
    void processError(const QString &s);
    void processTimeout(const QString &s);
    void visualizeSeries(bool checked, QLineSeries *series, QValueAxis *axisY);
    void visualizeSeriesRGBC(bool checked, QLineSeries *series, QValueAxis *axisY);

    void on_actionLicense_triggered();

private:
    void setControlsEnabled(bool enable);
    void createCommand();
    QChart* createChart();
    void parseResponse(const QString &response);
    void updateOnlineChart();
    void checkMax(QValueAxis *axisY, int currentValue);
    void checkMax(QValueAxis *axisY, double currentValue);
    void checkMin(QValueAxis *axisY, double currentValue);

private:
    Ui::DeepSeaGUI *ui;

    int m_transactionCount = 0;
    int m_responseCount = 0;
    QString m_measurementParameters;
    bool m_continuousMeasurement;
    MasterThread m_thread;
    QString m_currentFilePath;

    int m_redValue = 0;
    int m_greenValue = 0;
    int m_blueValue = 0;
    int m_clearValue = 0;
    int m_uvValue = 0;
    int m_irValue = 0.0;
    double m_pressureValue = 0.0;
    double m_temperatureValue = 0.0;

    QChart *m_onlineChart;
    QChartView *m_onlineChartView;
    QDateTimeAxis *m_axisX;
    QValueAxis *m_axisYrgbc;
    QValueAxis *m_axisYuv;
    QValueAxis *m_axisYir;
    QValueAxis *m_axisYpressure;
    QValueAxis *m_axisYtemperature;
    QLineSeries *m_seriesRed;
    QLineSeries *m_seriesGreen;
    QLineSeries *m_seriesBlue;
    QLineSeries *m_seriesClear;
    QLineSeries *m_seriesUV;
    QLineSeries *m_seriesIR;
    QLineSeries *m_seriesPressure;
    QLineSeries *m_seriesTemperature;

};

#endif // DEEPSEAGUI_H
