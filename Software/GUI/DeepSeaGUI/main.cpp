#include "deepseagui.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    DeepSeaGUI GUI;
    GUI.show();

    return app.exec();
}
