#include "masterthread.h"

#include <QSerialPort>
#include <QTime>

MasterThread::MasterThread(QObject *parent) :
    QThread(parent)
{
}

MasterThread::~MasterThread()
{
    m_mutex.lock();
    m_quit = true;
    m_cond.wakeOne();
    m_mutex.unlock();
    wait();
}

void MasterThread::transaction(const QString &portName, int waitTimeout,
                               const QString &request, int numberSamples)
{
    const QMutexLocker locker(&m_mutex);
    m_portName = portName;
    m_waitTimeout = waitTimeout;
    m_request = request;
    m_numberSamples = numberSamples;

    if (!isRunning())
        start();
    else
        m_cond.wakeOne();
}

void MasterThread::run()
{
    bool currentPortNameChanged = false;

    m_mutex.lock();

    QString currentPortName;
    if (currentPortName != m_portName) {
        currentPortName = m_portName;
        currentPortNameChanged = true;
    }

    int currentWaitTimeout = m_waitTimeout;
    QString currentRequest;
    currentRequest = m_request;

    m_mutex.unlock();

    QSerialPort serial;
    serial.setBaudRate(QSerialPort::Baud9600);

    if (currentPortName.isEmpty()) {
        emit error(tr("No port name specified"));
        return;
    }

    while (!m_quit) {
        if (currentPortNameChanged) {
            serial.close();
            serial.setPortName(currentPortName);

            if (!serial.open(QIODevice::ReadWrite)) {
                emit error(tr("Can't open %1, error code %2")
                           .arg(m_portName).arg(serial.error()));
                return;
            }
        }
        // write request
        const QByteArray requestData = currentRequest.toLatin1();
        serial.write(requestData);
        if (serial.waitForBytesWritten(m_waitTimeout)) {
            for (int i = 0; i < m_numberSamples; i++) {
            // read response

            if (serial.waitForReadyRead(currentWaitTimeout)) {
                QByteArray responseData = serial.readAll();
                while (serial.waitForReadyRead(10))
                    responseData += serial.readAll();

                const QString response = QString::fromLatin1(responseData);

                emit this->response(response);
            } else {
                if (m_request == ""){} //for requesting "" skip error
                else {
                    emit timeout(tr("Wait read response timeout %1")
                             .arg(QTime::currentTime().toString()));
                    serial.close();
                    return;
                }
            }
            }
        } else {
            if (m_request == ""){} //for requesting "" skip error
            else{
                emit timeout(tr("Wait write request timeout %1")
                         .arg(QTime::currentTime().toString()));
                serial.close();
                return;
            }
        }

        m_mutex.lock();
        m_cond.wait(&m_mutex);
        if (currentPortName != m_portName) {
            currentPortName = m_portName;
            currentPortNameChanged = true;
        } else {
            currentPortNameChanged = false;
        }
        currentWaitTimeout = m_waitTimeout;
        currentRequest = m_request;
        m_mutex.unlock();
    }
    serial.close();
}
