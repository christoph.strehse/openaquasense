/****************************************************************************
** Meta object code from reading C++ file 'deepseagui.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../DeepSeaGUI/deepseagui.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'deepseagui.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DeepSeaGUI_t {
    QByteArrayData data[41];
    char stringdata0[1207];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DeepSeaGUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DeepSeaGUI_t qt_meta_stringdata_DeepSeaGUI = {
    {
QT_MOC_LITERAL(0, 0, 10), // "DeepSeaGUI"
QT_MOC_LITERAL(1, 11, 36), // "on_m_serialPortRefreshButton_..."
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 47), // "on_m_TSL2572IntegrationtimeSp..."
QT_MOC_LITERAL(4, 97, 50), // "on_m_TCS3472RedIntegrationtim..."
QT_MOC_LITERAL(5, 148, 46), // "on_m_VEML6070IntegrationtimeC..."
QT_MOC_LITERAL(6, 195, 35), // "on_m_startMeasurementButton_c..."
QT_MOC_LITERAL(7, 231, 42), // "on_m_serialPortComboBox_curre..."
QT_MOC_LITERAL(8, 274, 35), // "on_m_sampleRateSpinBox_valueC..."
QT_MOC_LITERAL(9, 310, 38), // "on_m_numberSamplesSpinBox_val..."
QT_MOC_LITERAL(10, 349, 49), // "on_m_measurementMethodComboBo..."
QT_MOC_LITERAL(11, 399, 29), // "on_m_chooseFileButton_clicked"
QT_MOC_LITERAL(12, 429, 52), // "on_m_TCS3472GreenIntegrationt..."
QT_MOC_LITERAL(13, 482, 51), // "on_m_TCS3472BlueIntegrationti..."
QT_MOC_LITERAL(14, 534, 52), // "on_m_TCS3472ClearIntegrationt..."
QT_MOC_LITERAL(15, 587, 32), // "on_m_showIRCheckBox_stateChanged"
QT_MOC_LITERAL(16, 620, 32), // "on_m_showUVCheckBox_stateChanged"
QT_MOC_LITERAL(17, 653, 38), // "on_m_showPressureCheckBox_sta..."
QT_MOC_LITERAL(18, 692, 41), // "on_m_showTemperatureCheckBox_..."
QT_MOC_LITERAL(19, 734, 33), // "on_m_showRedCheckBox_stateCha..."
QT_MOC_LITERAL(20, 768, 35), // "on_m_showGreenCheckBox_stateC..."
QT_MOC_LITERAL(21, 804, 34), // "on_m_showBlueCheckBox_stateCh..."
QT_MOC_LITERAL(22, 839, 35), // "on_m_showClearCheckBox_stateC..."
QT_MOC_LITERAL(23, 875, 36), // "on_m_showPointsCheckBox_state..."
QT_MOC_LITERAL(24, 912, 39), // "on_m_legendVisibleCheckBox_st..."
QT_MOC_LITERAL(25, 952, 28), // "on_m_clearDataButton_clicked"
QT_MOC_LITERAL(26, 981, 28), // "on_m_saveChartButton_clicked"
QT_MOC_LITERAL(27, 1010, 34), // "on_m_stopMeasurementButton_cl..."
QT_MOC_LITERAL(28, 1045, 13), // "writeResponse"
QT_MOC_LITERAL(29, 1059, 8), // "response"
QT_MOC_LITERAL(30, 1068, 12), // "processError"
QT_MOC_LITERAL(31, 1081, 1), // "s"
QT_MOC_LITERAL(32, 1083, 14), // "processTimeout"
QT_MOC_LITERAL(33, 1098, 15), // "visualizeSeries"
QT_MOC_LITERAL(34, 1114, 7), // "checked"
QT_MOC_LITERAL(35, 1122, 12), // "QLineSeries*"
QT_MOC_LITERAL(36, 1135, 6), // "series"
QT_MOC_LITERAL(37, 1142, 11), // "QValueAxis*"
QT_MOC_LITERAL(38, 1154, 5), // "axisY"
QT_MOC_LITERAL(39, 1160, 19), // "visualizeSeriesRGBC"
QT_MOC_LITERAL(40, 1180, 26) // "on_actionLicense_triggered"

    },
    "DeepSeaGUI\0on_m_serialPortRefreshButton_clicked\0"
    "\0on_m_TSL2572IntegrationtimeSpinBox_valueChanged\0"
    "on_m_TCS3472RedIntegrationtimeSpinBox_valueChanged\0"
    "on_m_VEML6070IntegrationtimeComboBox_activated\0"
    "on_m_startMeasurementButton_clicked\0"
    "on_m_serialPortComboBox_currentTextChanged\0"
    "on_m_sampleRateSpinBox_valueChanged\0"
    "on_m_numberSamplesSpinBox_valueChanged\0"
    "on_m_measurementMethodComboBox_currentTextChanged\0"
    "on_m_chooseFileButton_clicked\0"
    "on_m_TCS3472GreenIntegrationtimeSpinBox_valueChanged\0"
    "on_m_TCS3472BlueIntegrationtimeSpinBox_valueChanged\0"
    "on_m_TCS3472ClearIntegrationtimeSpinBox_valueChanged\0"
    "on_m_showIRCheckBox_stateChanged\0"
    "on_m_showUVCheckBox_stateChanged\0"
    "on_m_showPressureCheckBox_stateChanged\0"
    "on_m_showTemperatureCheckBox_stateChanged\0"
    "on_m_showRedCheckBox_stateChanged\0"
    "on_m_showGreenCheckBox_stateChanged\0"
    "on_m_showBlueCheckBox_stateChanged\0"
    "on_m_showClearCheckBox_stateChanged\0"
    "on_m_showPointsCheckBox_stateChanged\0"
    "on_m_legendVisibleCheckBox_stateChanged\0"
    "on_m_clearDataButton_clicked\0"
    "on_m_saveChartButton_clicked\0"
    "on_m_stopMeasurementButton_clicked\0"
    "writeResponse\0response\0processError\0"
    "s\0processTimeout\0visualizeSeries\0"
    "checked\0QLineSeries*\0series\0QValueAxis*\0"
    "axisY\0visualizeSeriesRGBC\0"
    "on_actionLicense_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DeepSeaGUI[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      32,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  174,    2, 0x08 /* Private */,
       3,    0,  175,    2, 0x08 /* Private */,
       4,    0,  176,    2, 0x08 /* Private */,
       5,    0,  177,    2, 0x08 /* Private */,
       6,    0,  178,    2, 0x08 /* Private */,
       7,    0,  179,    2, 0x08 /* Private */,
       8,    0,  180,    2, 0x08 /* Private */,
       9,    0,  181,    2, 0x08 /* Private */,
      10,    0,  182,    2, 0x08 /* Private */,
      11,    0,  183,    2, 0x08 /* Private */,
      12,    0,  184,    2, 0x08 /* Private */,
      13,    0,  185,    2, 0x08 /* Private */,
      14,    0,  186,    2, 0x08 /* Private */,
      15,    0,  187,    2, 0x08 /* Private */,
      16,    0,  188,    2, 0x08 /* Private */,
      17,    0,  189,    2, 0x08 /* Private */,
      18,    0,  190,    2, 0x08 /* Private */,
      19,    0,  191,    2, 0x08 /* Private */,
      20,    0,  192,    2, 0x08 /* Private */,
      21,    0,  193,    2, 0x08 /* Private */,
      22,    0,  194,    2, 0x08 /* Private */,
      23,    0,  195,    2, 0x08 /* Private */,
      24,    0,  196,    2, 0x08 /* Private */,
      25,    0,  197,    2, 0x08 /* Private */,
      26,    0,  198,    2, 0x08 /* Private */,
      27,    0,  199,    2, 0x08 /* Private */,
      28,    1,  200,    2, 0x08 /* Private */,
      30,    1,  203,    2, 0x08 /* Private */,
      32,    1,  206,    2, 0x08 /* Private */,
      33,    3,  209,    2, 0x08 /* Private */,
      39,    3,  216,    2, 0x08 /* Private */,
      40,    0,  223,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   29,
    QMetaType::Void, QMetaType::QString,   31,
    QMetaType::Void, QMetaType::QString,   31,
    QMetaType::Void, QMetaType::Bool, 0x80000000 | 35, 0x80000000 | 37,   34,   36,   38,
    QMetaType::Void, QMetaType::Bool, 0x80000000 | 35, 0x80000000 | 37,   34,   36,   38,
    QMetaType::Void,

       0        // eod
};

void DeepSeaGUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DeepSeaGUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_m_serialPortRefreshButton_clicked(); break;
        case 1: _t->on_m_TSL2572IntegrationtimeSpinBox_valueChanged(); break;
        case 2: _t->on_m_TCS3472RedIntegrationtimeSpinBox_valueChanged(); break;
        case 3: _t->on_m_VEML6070IntegrationtimeComboBox_activated(); break;
        case 4: _t->on_m_startMeasurementButton_clicked(); break;
        case 5: _t->on_m_serialPortComboBox_currentTextChanged(); break;
        case 6: _t->on_m_sampleRateSpinBox_valueChanged(); break;
        case 7: _t->on_m_numberSamplesSpinBox_valueChanged(); break;
        case 8: _t->on_m_measurementMethodComboBox_currentTextChanged(); break;
        case 9: _t->on_m_chooseFileButton_clicked(); break;
        case 10: _t->on_m_TCS3472GreenIntegrationtimeSpinBox_valueChanged(); break;
        case 11: _t->on_m_TCS3472BlueIntegrationtimeSpinBox_valueChanged(); break;
        case 12: _t->on_m_TCS3472ClearIntegrationtimeSpinBox_valueChanged(); break;
        case 13: _t->on_m_showIRCheckBox_stateChanged(); break;
        case 14: _t->on_m_showUVCheckBox_stateChanged(); break;
        case 15: _t->on_m_showPressureCheckBox_stateChanged(); break;
        case 16: _t->on_m_showTemperatureCheckBox_stateChanged(); break;
        case 17: _t->on_m_showRedCheckBox_stateChanged(); break;
        case 18: _t->on_m_showGreenCheckBox_stateChanged(); break;
        case 19: _t->on_m_showBlueCheckBox_stateChanged(); break;
        case 20: _t->on_m_showClearCheckBox_stateChanged(); break;
        case 21: _t->on_m_showPointsCheckBox_stateChanged(); break;
        case 22: _t->on_m_legendVisibleCheckBox_stateChanged(); break;
        case 23: _t->on_m_clearDataButton_clicked(); break;
        case 24: _t->on_m_saveChartButton_clicked(); break;
        case 25: _t->on_m_stopMeasurementButton_clicked(); break;
        case 26: _t->writeResponse((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: _t->processError((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 28: _t->processTimeout((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 29: _t->visualizeSeries((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< QLineSeries*(*)>(_a[2])),(*reinterpret_cast< QValueAxis*(*)>(_a[3]))); break;
        case 30: _t->visualizeSeriesRGBC((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< QLineSeries*(*)>(_a[2])),(*reinterpret_cast< QValueAxis*(*)>(_a[3]))); break;
        case 31: _t->on_actionLicense_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 29:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLineSeries* >(); break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QValueAxis* >(); break;
            }
            break;
        case 30:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLineSeries* >(); break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QValueAxis* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DeepSeaGUI::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_DeepSeaGUI.data,
    qt_meta_data_DeepSeaGUI,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DeepSeaGUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DeepSeaGUI::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DeepSeaGUI.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int DeepSeaGUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 32)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 32;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 32)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 32;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
