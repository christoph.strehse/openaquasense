/*  Copyright © 2021 Christoph Strehse
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Wire.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <SD.h>
#include <TimeLib.h>

//settings for status LED (buttonLED)
#define buttonLED 7
unsigned long previousMillis = 0;
const long intervalBlink = 1000;

//i2c Addresses
static const uint8_t LP55231_I2C = 0x32;
static const uint8_t TCA9548A_I2C = 0x70;
static const uint8_t TCS3472_I2C = 0x29;
static const uint8_t TSL2572_I2C = 0x39;
static const uint8_t VEML6070_I2C_H = 0x39; //for read operations
static const uint8_t VEML6070_I2C_L = 0x38; //for read and write operations
static const uint8_t MS5803_I2C = 0x76;
static const uint8_t VEML6070_ADDR_ARA = 0x0C; //Alert Resp Address (read to clear condition)

//LP5231 Registers
//Enable register
static const uint8_t LP55231_REG_CNTRL1 = 0x00;
//Reset register
static const uint8_t LP55231_REG_RESET = 0x3D;
//MISC register: [0] Clock source selection, [1] External clock detection, 
//[2] PWM cycle powersave enable, [3,4] Charge pump gain selection, 
//[5] Powersave mode enable, [6] Serial bus address auto increment enable, 
//[7] Variable D source selection)
static const uint8_t LP55231_REG_MISC = 0x36;
//Output ON/OFF control register
static const uint8_t LP55231_REG_OUTPUT_ONOFF_MSB = 0x04;
static const uint8_t LP55231_REG_OUTPUT_ONOFF_LSB = 0x05;
//ratiometric dimming enable
/*static const uint8_t LP55231_REG_RATIOMETRIC_DIMMING_MSB = 0x02;
static const uint8_t LP55231_REG_RATIOMETRIC_DIMMING_LSB = 0x03;*/
//PWM duty cycle control register
static const uint8_t LP55231_REG_D8_PWM = 0x1D; //UV
static const uint8_t LP55231_REG_D5_PWM = 0x1A; //green
static const uint8_t LP55231_REG_D6_PWM = 0x1B; //blue
static const uint8_t LP55231_REG_D7_PWM = 0x1C; //IR
static const uint8_t LP55231_REG_D9_PWM = 0x1E; //red
//output current control register
static const uint8_t LP55231_REG_D8_I_CTL = 0x2D; //UV
static const uint8_t LP55231_REG_D5_I_CTL = 0x2A; //green
static const uint8_t LP55231_REG_D6_I_CTL = 0x2B; //blue
static const uint8_t LP55231_REG_D7_I_CTL = 0x2C; //IR
static const uint8_t LP55231_REG_D9_I_CTL = 0x2E; //red

//TCS3472 registers
static const uint8_t TCS3472_ENABLE = 0x00; //power on/off, enable functions, interrupt settings
static const uint8_t TCS3472_RGBC_TIME = 0x01; //RGBC integration time
static const uint8_t TCS3472_WAIT_TIME = 0x03; //Wait time register
static const uint8_t TCS3472_CONFIG = 0x0D; //wait long
static const uint8_t TCS3472_RGBC_GAIN = 0x0F; //RGBC gain
static const uint8_t TCS3472_CDATAL = 0x14; //clear channel data low byte
static const uint8_t TCS3472_CDATAH = 0x15; //clear channel data high byte
static const uint8_t TCS3472_RDATAL = 0x16; //red channel data low byte
static const uint8_t TCS3472_RDATAH = 0x17; //red channel data high byte
static const uint8_t TCS3472_GDATAL = 0x18; //green channel data low byte
static const uint8_t TCS3472_GDATAH = 0x19; //green channel data high byte
static const uint8_t TCS3472_BDATAL = 0x1A; //blue channel data low byte
static const uint8_t TCS3472_BDATAH = 0x1B; //blue channel data high byte

//TCS3472 command settings
static const uint8_t TCS3472_REPEATED_BYTE = 0x80;  //repeateadly read same register
static const uint8_t TCS3472_AUTO_INCREMENT = 0xA0; //auto-increment register pointer
static const uint8_t TCS3472_COMMAND_BIT = 0x80; //command bit

//TCS3472 gain settings
static const uint8_t TSC3472_GAIN_1X = 0;
static const uint8_t TSC3472_GAIN_4X = 1;
static const uint8_t TSC3472_GAIN_16X = 2;
static const uint8_t TSC3472_GAIN_60X = 3;

//TSL2572 registers
static const uint8_t TSL2572_ENABLE = 0x00; //power on/off, enable functions, interrupt settings
static const uint8_t TSL2572_ALS_TIME = 0x01; //ALS integration time
static const uint8_t TSL2572_CONFIG = 0x0D; //set wait long and ALS gain level (1/6 for 1X, 8X)
static const uint8_t TSL2572_ALS_GAIN = 0x0F; //ALS gain
static const uint8_t TSL2572_DATA = 0x14; //Data Register C0 (0x14, 0x15)
//static const uint8_t TSL2572_DATA_C1 = 0x16; //Data Register C1 (0x16, 0x17)

//TSL2572 command settings
static const uint8_t TSL2572_REPEATED_BYTE = 0x80;  //repeateadly read same register
static const uint8_t TSL2572_AUTO_INCREMENT = 0xA0; //auto-increment register pointer
static const uint8_t TSL2572_COMMAND_BIT = 0x80; //command bit

//TSL2572 gain settings
static const uint8_t TSL2572_GAIN_1X = 0;
static const uint8_t TSL2572_GAIN_8X = 1;
static const uint8_t TSL2572_GAIN_16X = 2;
static const uint8_t TSL2572_GAIN_120X = 3;

//TSL2572 gain 1/6
bool _TSL2572_gainDivide_6 = false;

//VEML6070 Shutdown mode setting
static const uint8_t VEML6070_SD_DISABLE = 0x00;
static const uint8_t VEML6070_SD_ENABLE = 0x01;

//VEML6070 integration time settings
static const uint8_t VEML6070_INT_HALF = 0x00;
static const uint8_t VEML6070_INT_1 = 0x04;
static const uint8_t VEML6070_INT_2 = 0x08;
static const uint8_t VEML6070_INT_4 = 0x0C;

//VEML6070 current command register content
uint8_t _VEML6070_command_register = 0x00;

//MS5803 Commands
//Reset command
static const uint8_t MS5803_CMD_RESET = 0x1E;
//ADC read command
static const uint8_t MS5803_CMD_ADC_READ = 0x00;
//ADC conversion command
static const uint8_t MS5803_CMD_ADC_CONVERT = 0x40;
//Coefficcient location
static const uint8_t MS5803_CMD_PROM = 0xA0;

//ADC Precision
static const uint8_t MS5803_ADC_256 = 0x00;
static const uint8_t MS5803_ADC_512 = 0x02;
static const uint8_t MS5803_ADC_1024 = 0x04;
static const uint8_t MS5803_ADC_2048 = 0x06;
static const uint8_t MS5803_ADC_4096 = 0x08;

//MS5803 measurement value
static const uint8_t PRESSURE = 0x00;
static const uint8_t TEMPERATURE = 0x10;

//MS5803 calibration coefficients
uint16_t _MS5803_coefficients[8];

//pressure and temperature value
int32_t _temperature;
int32_t _pressure;

const String actualTime;
const String Now;

//measurement parameters sent by GUI
uint8_t pwmUV = 0, currentUV = 0, pwmIR = 0, currentIR = 0;
uint8_t pwmR = 0, currentR = 0, pwmG = 0, currentG = 0, pwmB = 0, currentB = 0; 
uint8_t TSL2572intTime = 0, TSL2572gain = 0;
uint8_t TCS3472redIntTime = 0, TCS3472redGain = 0; 
uint8_t TCS3472greenIntTime = 0, TCS3472greenGain = 0;
uint8_t TCS3472blueIntTime = 0, TCS3472blueGain = 0;
uint8_t TCS3472clearIntTime = 0, TCS3472clearGain = 0;
uint8_t VEML6070intTime = 0;
uint8_t MS5803pressureADC = 0, MS5803temperatureADC = 0;
int numberSamples = 0, timeBetweenSamples = 0;
bool continuousMeasurement = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  //USB
  Serial2.begin(9600); //Bluetooth
  while(!Serial) continue;
  Wire.begin();

  logToSD("Start initialisation of OAS_GUI");
  
  LP55231_ledsOff();
  LP55231_disable();
  TSL2572_disable();
  VEML6070_disable();
  TCS3472_disable();
  TCA9548A_disable();
  MS5803_reset();

  logToSD("Initialisation done");

}

void loop() {
  // put your main code here, to run repeatedly:

  //do nothing until parameters are sent by GUI for the first loop
  if (continuousMeasurement == false) while(!Serial.available()) continue;

  //read parameters sent by GUI
  String parameters;
  if(Serial.available()){ 
    parameters = Serial.readString(); //Until('/');
    //Serial.println(parameters);
   // Serial.flush();
  }

  //parse string to measurement parameters
  parseParameters(parameters, &continuousMeasurement, &pwmUV, &currentUV, &pwmIR, 
                  &currentIR, &pwmR, &currentR, &pwmG, &currentG, &pwmB, &currentB, 
                  &TSL2572intTime, &TSL2572gain, &TCS3472redIntTime, &TCS3472greenIntTime, 
                  &TCS3472blueIntTime, &TCS3472clearIntTime, &TCS3472redGain,
                  &TCS3472greenGain, &TCS3472blueGain, &TCS3472clearGain, &VEML6070intTime, 
                  &MS5803pressureADC, &MS5803temperatureADC, &numberSamples, 
                  &timeBetweenSamples);
                    
  for(int i = 0; i < numberSamples; i++){  

    logToSD("Begin measurement");
    
    //Serial.print("Sample #"); Serial.println(i+1);
    //determine actual time
    actualTime = timeStamp();
    Now = now();
  
    //acknowledge for user feedback (buttonLED)
    ackn(2, 50);
    digitalWrite(buttonLED, HIGH);   
     
    //enable LP55231 and light IR
    LP55231_enable();
    LP55231_lightIR(pwmIR, currentIR);
    //enable TSL2572, get IR values and disable
    TCA9548A_enableTSL2572();
    uint16_t ir;
    TSL2572_getIR(TSL2572gain, TSL2572intTime, &ir);
   // TSL2572_disable();
    TCA9548A_disable();
    //turn off LEDs and get LP55231 to sleep mod
    LP55231_ledsOff();
    LP55231_disable();
  
    //enable LP55231 and light UV
    LP55231_enable();
    LP55231_lightUV(pwmUV, currentUV);
    //enable VEML6070 and get UV values
    TCA9548A_enableVEML6070();
    uint16_t uv;
    VEML6070_getUV(VEML6070intTime, &uv);
   // VEML6070_disable();
    TCA9548A_disable();
    //turn off LEDs and get LP55231 to sleep mod
    LP55231_ledsOff();
    LP55231_disable();
  
    //enable LP55231 and light R
    LP55231_enable();
    LP55231_lightRed(pwmR, currentR);  
    //enable TCS3472, get R value and disable
    TCA9548A_enableTCS3472();
    uint16_t r;
    TCS3472_getR(TCS3472redGain, TCS3472redIntTime, &r);
   // TCS3472_disable();  
    TCA9548A_disable();
    //turn off LEDs and get LP55231 to sleep mod
    LP55231_ledsOff();
    LP55231_disable();   
  
    //enable LP55231 and light G
    LP55231_enable();
    LP55231_lightGreen(pwmG, currentG);  
    //enable TCS3472, get R value and disable
    TCA9548A_enableTCS3472();
    uint16_t g;
    TCS3472_getG(TCS3472greenGain, TCS3472greenIntTime, &g);
   // TCS3472_disable();  
    TCA9548A_disable(); 
    //turn off LEDs and get LP55231 to sleep mod
    LP55231_ledsOff();
    LP55231_disable();
    
    //enable LP55231 and light B
    LP55231_enable();
    LP55231_lightBlue(pwmB, currentB);  
    //enable TCS3472, get B value and disable
    TCA9548A_enableTCS3472();
    uint16_t b;
    TCS3472_getB(TCS3472blueGain, TCS3472blueIntTime, &b);
   // TCS3472_disable();  
    TCA9548A_disable();
    //turn off LEDs and get LP55231 to sleep mod
    LP55231_ledsOff();
    LP55231_disable();
  
    //get C value without light
    //enable TCS3472, get C value and disable
    TCA9548A_enableTCS3472();
    uint16_t c;
    TCS3472_getC(TCS3472clearGain, TCS3472clearIntTime, &c);
   // TCS3472_disable();  
    TCA9548A_disable();
  
    //get pressure and temperature vale
    MS5803_reset();
    MS5803_enable();
    float pressure = MS5803_getPressure(MS5803pressureADC);
    float temperature = MS5803_getTemperature(MS5803temperatureADC);

    logToSD("Measurement done");
  
    //make JSON string from Data
    char jsonString[1000];
    createJSON(&uv, &ir, &r, &g, &b, &c, &pressure, &temperature, &pwmUV, &currentUV,
              &pwmIR, &currentIR, &pwmR, &currentR, &pwmG, &currentG, &pwmB, &currentB,
              &TSL2572intTime, &TSL2572gain, &TCS3472redIntTime, &TCS3472redGain,
              &TCS3472greenIntTime, &TCS3472greenGain, &TCS3472blueIntTime, &TCS3472blueGain,
              &TCS3472clearIntTime, &TCS3472clearGain, &VEML6070intTime, &MS5803pressureADC,
              &MS5803temperatureADC, jsonString);  

    //acknowledge for user feedback
    ackn(5,50);
  
    //print measurement parameters for debugging purpose
    /*
    Serial.print(pwmUV); Serial.print(","); Serial.print(currentUV); Serial.print(","); 
    Serial.print(pwmIR); Serial.print(","); Serial.print(currentIR); Serial.print(",");
    Serial.print(pwmR); Serial.print(","); Serial.print(currentR); Serial.print(",");
    Serial.print(pwmG); Serial.print(","); Serial.print(currentG); Serial.print(",");
    Serial.print(pwmB); Serial.print(","); Serial.print(currentB); Serial.print(",");
    Serial.print(TSL2572intTime); Serial.print(","); Serial.print(TSL2572gain); Serial.print(",");
    Serial.print(TCS3472redIntTime); Serial.print(","); Serial.print(TCS3472greenIntTime); Serial.print(",");
    Serial.print(TCS3472blueIntTime); Serial.print(","); Serial.print(TCS3472clearIntTime); Serial.print(",");
    Serial.print(TCS3472redGain); Serial.print(","); Serial.print(TCS3472greenGain); Serial.print(","); 
    Serial.print(TCS3472blueGain); Serial.print(","); Serial.print(TCS3472clearGain); Serial.print(",");
    Serial.print(VEML6070intTime); Serial.print(","); Serial.print(MS5803pressureADC); Serial.print(",");
    Serial.print(MS5803temperatureADC); Serial.print(","); Serial.print(continuousMeasurement); Serial.print(",");
    Serial.print(numberSamples); Serial.print(","); Serial.print(timeBetweenSamples); Serial.println(",");
    */
    
    //send measured values back to GUI
    Serial.println(jsonString);
    Serial2.println(jsonString);
      
    //write JSON string to .txt File
    writeToSD(jsonString);  
    logToSD(jsonString);
    
    delay((timeBetweenSamples * 1000) - 6000);
  }
  continuousMeasurement = false;
}

//***********************************************************
//*                   other Functions                       *
//***********************************************************

void parseParameters(String parameters, bool* continuousMeasurement, uint8_t* pwmUV, 
                     uint8_t* currentUV, uint8_t* pwmIR, uint8_t* currentIR, uint8_t* pwmR, 
                     uint8_t* currentR, uint8_t* pwmG, uint8_t* currentG, uint8_t* pwmB, 
                     uint8_t* currentB, uint8_t* TSL2572intTime, uint8_t* TSL2572gain,
                     uint8_t* TCS3472redIntTime, uint8_t* TCS3472greenIntTime,
                     uint8_t* TCS3472blueIntTime, uint8_t* TCS3472clearIntTime, 
                     uint8_t* TCS3472redGain, uint8_t* TCS3472greenGain, 
                     uint8_t* TCS3472blueGain, uint8_t* TCS3472clearGain, 
                     uint8_t* VEML6070intTime, uint8_t* MS5803pressureADC, 
                     uint8_t* MS5803temperatureADC, uint16_t* numberSamples, 
                     uint16_t* timeBetweenSamples){
  
  String strTSL2572gain = "", strTCS3472gain = "", strVEML6070intTime = "";
  String strMS5803pressureADC = "", strMS5803temperatureADC = "", strContinuousMeasurement = "";
  int commaCount = 0;
  int currentComma = 0;
  int lastComma = 0;
  
  for(int i = 1; i < parameters.length() + 1; i++){
    if(parameters[i] == ','){
      lastComma = currentComma;
      currentComma = i;
      commaCount ++;

      switch(commaCount){
        case 1:
          *pwmUV = parameters.substring(0, currentComma).toInt();
          break;
        case 2:
          *currentUV = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 3:
          *pwmIR = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 4: 
          *currentIR = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 5:
          *pwmR = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 6:
          *currentR = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 7:
          *pwmG = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 8:
          *currentG = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 9:
          *pwmB = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 10:
          *currentB = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 11:
          *TSL2572intTime = 256 - parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 12:
          strTSL2572gain = parameters.substring(lastComma + 1, currentComma);
          if(strTSL2572gain == "1x") *TSL2572gain = TSL2572_GAIN_1X;
          else if(strTSL2572gain == "8x") *TSL2572gain = TSL2572_GAIN_8X;
          else if(strTSL2572gain == "16x") *TSL2572gain = TSL2572_GAIN_16X;
          else if(strTSL2572gain == "120x") *TSL2572gain = TSL2572_GAIN_120X;
          break;
        case 13:
          *TCS3472redIntTime = 256 - parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 14:
          *TCS3472greenIntTime = 256 - parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 15:
          *TCS3472blueIntTime = 256 - parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 16:
          *TCS3472clearIntTime = 256 - parameters.substring(lastComma + 1, currentComma).toInt();
          break;                    
        case 17: 
          strTCS3472gain = parameters.substring(lastComma + 1, currentComma);
          if(strTCS3472gain == "1x") *TCS3472redGain = TSC3472_GAIN_1X;
          else if(strTCS3472gain == "4x") *TCS3472redGain = TSC3472_GAIN_4X;
          else if(strTCS3472gain == "16x") *TCS3472redGain = TSC3472_GAIN_16X;
          else if(strTCS3472gain == "60x") *TCS3472redGain = TSC3472_GAIN_60X;
          break;
        case 18: 
          strTCS3472gain = parameters.substring(lastComma + 1, currentComma);
          if(strTCS3472gain == "1x") *TCS3472greenGain = TSC3472_GAIN_1X;
          else if(strTCS3472gain == "4x") *TCS3472greenGain = TSC3472_GAIN_4X;
          else if(strTCS3472gain == "16x") *TCS3472greenGain = TSC3472_GAIN_16X;
          else if(strTCS3472gain == "60x") *TCS3472greenGain = TSC3472_GAIN_60X;
          break;
        case 19: 
          strTCS3472gain = parameters.substring(lastComma + 1, currentComma);
          if(strTCS3472gain == "1x") *TCS3472blueGain = TSC3472_GAIN_1X;
          else if(strTCS3472gain == "4x") *TCS3472blueGain = TSC3472_GAIN_4X;
          else if(strTCS3472gain == "16x") *TCS3472blueGain = TSC3472_GAIN_16X;
          else if(strTCS3472gain == "60x") *TCS3472blueGain = TSC3472_GAIN_60X;
          break; 
        case 20: 
          strTCS3472gain = parameters.substring(lastComma + 1, currentComma);
          if(strTCS3472gain == "1x") *TCS3472clearGain = TSC3472_GAIN_1X;
          else if(strTCS3472gain == "4x") *TCS3472clearGain = TSC3472_GAIN_4X;
          else if(strTCS3472gain == "16x") *TCS3472clearGain = TSC3472_GAIN_16X;
          else if(strTCS3472gain == "60x") *TCS3472clearGain = TSC3472_GAIN_60X;
          break;                   
        case 21: 
          strVEML6070intTime = parameters.substring(lastComma + 1, currentComma);
          if(strVEML6070intTime == "0.5x") *VEML6070intTime = VEML6070_INT_HALF;
          else if(strVEML6070intTime == "1x") *VEML6070intTime = VEML6070_INT_1;
          else if(strVEML6070intTime == "2x") *VEML6070intTime = VEML6070_INT_2;
          else if(strVEML6070intTime == "4x") *VEML6070intTime = VEML6070_INT_4;
          break;
        case 22: 
          strMS5803pressureADC = parameters.substring(lastComma + 1, currentComma);
          if(strMS5803pressureADC == "256") *MS5803pressureADC = MS5803_ADC_256;
          else if(strMS5803pressureADC == "512") *MS5803pressureADC = MS5803_ADC_512;
          else if(strMS5803pressureADC == "1024") *MS5803pressureADC = MS5803_ADC_1024;
          else if(strMS5803pressureADC == "2048") *MS5803pressureADC = MS5803_ADC_2048;
          else if(strMS5803pressureADC == "4096") *MS5803pressureADC = MS5803_ADC_4096;
          break;
        case 23: 
          strMS5803temperatureADC = parameters.substring(lastComma + 1, currentComma);
          if(strMS5803temperatureADC == "256") *MS5803temperatureADC = MS5803_ADC_256;
          else if(strMS5803temperatureADC == "512") *MS5803temperatureADC = MS5803_ADC_512;
          else if(strMS5803temperatureADC == "1024") *MS5803temperatureADC = MS5803_ADC_1024;
          else if(strMS5803temperatureADC == "2048") *MS5803temperatureADC = MS5803_ADC_2048;
          else if(strMS5803temperatureADC == "4096") *MS5803temperatureADC = MS5803_ADC_4096;
          break;
        case 24:
          strContinuousMeasurement = parameters.substring(lastComma + 1, currentComma);
          if (strContinuousMeasurement == "Single Measurement") 
            *continuousMeasurement = false;
          else if (strContinuousMeasurement == "Continuous Measurement") 
            *continuousMeasurement = true;
          break;
        case 25:
          *numberSamples = parameters.substring(lastComma + 1, currentComma).toInt();
          break;
        case 26:
          *timeBetweenSamples = parameters.substring(lastComma + 1, currentComma).toInt();    
          break;                    
      }
    }
  }
}

String timeStamp() {
  char timeStamp[32];
  sprintf(timeStamp, "%02d-%02d-%02dT%02d:%02d:%02d",
          year(), month(), day(), hour(), minute(), second());
  return timeStamp;
}

//function for buttonLED (green) user feedback; you can set number of flashes and frequency
void ackn(int amount, int waitTime) {
  for (int i = 0; i< amount; i++) {
    digitalWrite(buttonLED, HIGH);
    delay(waitTime);
    digitalWrite(buttonLED, LOW);
    delay(waitTime);
  }
}

//***********************************************************
//*                 JSON Functions                          *
//***********************************************************

void createJSON(uint16_t *uv, uint16_t *ir, uint16_t *r, uint16_t *g, 
                uint16_t *b, uint16_t *c, float *pressure, float *temperature,
                uint8_t *pwmUV, uint8_t *currentUV, 
                uint8_t *pwmIR, uint8_t *currentIR,
                uint8_t *pwmR, uint8_t *currentR, 
                uint8_t *pwmG, uint8_t *currentG, 
                uint8_t *pwmB, uint8_t *currentB,
                uint8_t *TSL2572intTime, uint8_t *TSL2572gain,
                uint8_t *TCS3472redIntTime, uint8_t *TCS3472redGain, 
                uint8_t *TCS3472greenIntTime, uint8_t *TCS3472greenGain,
                uint8_t *TCS3472blueIntTime, uint8_t *TCS3472blueGain,
                uint8_t *TCS3472clearIntTime, uint8_t *TCS3472clearGain,
                uint8_t *VEML6070intTime,
                uint8_t *MS5803pressureADC, uint8_t *MS5803temperatureADC,
                char *jsonString){
  
  const size_t capacity = JSON_OBJECT_SIZE(33) + 1000;
  DynamicJsonDocument doc(capacity);

  doc["Time"] = actualTime;
  doc["UTC"] = Now;  
  doc["UV"] = *uv;
  doc["IR"] = *ir;
  doc["R"] = *r;
  doc["G"] = *g;
  doc["B"] = *b;
  doc["C"] = *c;
  doc["Pressure"] = *pressure;
  doc["Temperature"] = *temperature;
  doc["pwmUV"] = *pwmUV;
  doc["currentUV"] = *currentUV;
  doc["pwmIR"] = *pwmIR;
  doc["currentIR"] = *currentIR;
  doc["pwmR"] = *pwmR;
  doc["currentR"] = *currentR;
  doc["pwmG"] = *pwmG;
  doc["currentG"] = *currentG;
  doc["pwmB"] = *pwmB;
  doc["currentB"] = *currentB;
  doc["TSL2572intTime"] = *TSL2572intTime;
  doc["TSL2572gain"] = *TSL2572gain;
  doc["TCS3472redIntTime"] = *TCS3472redIntTime;
  doc["TCS3472redGain"] = *TCS3472redGain;
  doc["TCS3472greenIntTime"] = *TCS3472greenIntTime;
  doc["TCS3472greenGain"] = *TCS3472greenGain;
  doc["TCS3472blueIntTime"] = *TCS3472blueIntTime;
  doc["TCS3472blueGain"] = *TCS3472blueGain;
  doc["TCS3472clearIntTime"] = *TCS3472clearIntTime;
  doc["TCS3472clearGain"] = *TCS3472clearGain;
  doc["VEML6070intTime"] = *VEML6070intTime;
  doc["MS5803pressureADC"] = *MS5803pressureADC;
  doc["MS5803temperatureADC"] = *MS5803temperatureADC;

  if(!serializeJson(doc, jsonString, 1000)){
    Serial.println("Error creating JSON string");
    Serial2.println("Error creating JSON string");
    logToSD("Error creating JSON string");
  }
}

//***********************************************************
//*                 SD-Card Functions                       *
//***********************************************************

void writeToSD(char *jsonString){
//open .txt File, writes the jsonString and close .txt
  File measurementFile;

  //inizialize SD Card
  if (!SD.begin(4)) {
   //Serial.println("SD Card initialization failed!");
   //Serial2.println("SD Card initialization failed!");
   logToSD("SD Card initialization for Datafile failed!");
   // while (1);
  }
  //Serial.println("initialization done.");
 
  //open measurementFile
  measurementFile = SD.open("data.txt", FILE_WRITE);

  //if File opened successfully, write jsonString
  if(measurementFile){
    measurementFile.println(jsonString);
    //Serial.println("JSON String written to Datafile");
    //Serial2.println("JSON String written to Datafile");
    logToSD("JSON String written to Datafile");
    //close measurementFile
    measurementFile.close();
  }
  else {
    //if measurementFile did not open, print error
    //Serial.println("SD Card Error opening Datafile.");
    //Serial2.println("SD Card Error opening Datafile.");
    logToSD("SD Card Error opening Datafile.");  
  }
}


//write message to Logfile on SD Card
void logToSD(char *logMessage){
  File logFile;

  //inizialize SD Card
  if (!SD.begin(4)) {
    //Serial.println("Error, SD Card initialization for Logfile failed!");
    //Serial2.println("Error, SD Card initialization for Logfile failed!");
  }// else Serial.println("SD Card inizialization for Logfile done");

  //open measurementFile with the name "log.txt"
  logFile = SD.open("log.txt", FILE_WRITE);

  //if the file opened successfully, write logMessage
  if(logFile){
    actualTime = timeStamp();
    logFile.print(actualTime);
    logFile.print(": ");
    logFile.println(logMessage);
    
    //Serial.println("Log message written to Logfile");
    //Serial2.println("Log message written to Logfile");

    //close logFile
    logFile.close();
  }
  //if logFile did not open, print error to serial monitor
  else{ 
    //Serial.println("SD Card Error opening Logfile");
    //Serial2.println("SD Card Error opening Logfile");
  }
}


//***********************************************************
//*                 I2C Functions                           *
//***********************************************************

void i2cWrite(uint8_t adress, uint8_t reg, int value){
//Write data on i2c-device
  Wire.beginTransmission(adress);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();

  //wait 10 ms for Transmission
  delay(10);
}

void i2cWrite(uint8_t adress, int value){
//Write data on i2c-device
  Wire.beginTransmission(adress);
  Wire.write(value);
  Wire.endTransmission();
  
  //wait 10 ms for Transmission
  delay(10);
}

int i2cRead(uint8_t adress, uint8_t reg, int bytes){
//read data from i2c-Device
  int result = 0;
  
  //Pointer on register
  Wire.beginTransmission(adress);
  Wire.write(reg);
  Wire.endTransmission();
  
  //wait 10 ms for Transmission
  delay(10);

  //read data until recieving buffer is empty
  Wire.requestFrom(adress, bytes);
  while(Wire.available()){
    uint8_t i;
    uint8_t buff = 0;
    for (i = 0; i < bytes; i++){
      buff = Wire.read();
      result = (result << 8) | buff;
    }
  }
  return result;
}

int i2cRead(uint8_t adress, int bytes){
//read data from i2c-Device
  uint16_t result = 0;
  
  //Pointer on register
  //Wire.beginTransmission(adress);
  //Wire.write(reg);
  //Wire.endTransmission(false);

  //wait 10 ms for Transmission
  //delay(10);

  //read data until recieving buffer is empty
  Wire.requestFrom(adress, bytes);

  //read every byte and calculate result
  while(Wire.available()){
    uint8_t i;
    uint8_t buff = 0;
    for (i = 0; i < bytes; i++){
      buff = Wire.read();
      result = (result << 8) | buff;
    }
  } 
  return result;
}

//***********************************************************
//*                 I2C-Switch Functions                    *
//***********************************************************

void TCA9548A_enableVEML6070(){
//enables Channel 0 of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x01);
 // Serial.println("TCA9548A channel 0 enabled"); 
}

void TCA9548A_enableTCS3472(){
//enables Channel 1 of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x02);
 // Serial.println("TCA9548A channel 1 enabled"); 
}

void TCA9548A_enableTSL2572(){
//enables Channel 2 of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x04);
 // Serial.println("TCA9548A channel 2 enabled"); 
}

void TCA9548A_enableAll(){
//enables Channel 0, 1, 2 of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x07);
 // Serial.println("TCA9548A channels 0, 1, 2 enabled"); 
}

void TCA9548A_disable(){
//disables Channels of TCA9548A
  i2cWrite(TCA9548A_I2C, 0x00);
 // Serial.println("TCA9548A all channels disabled"); 
}

//***********************************************************
//*                 LP55231 Functions                       *
//***********************************************************

void LP55231_enable(){
//set enable bit of LP55231
  i2cWrite(LP55231_I2C, LP55231_REG_CNTRL1, 0x40);
  //enable internal clock & charge pump & write auto increment 
  i2cWrite(LP55231_I2C, LP55231_REG_MISC, 0x53);
}

void LP55231_disable(){
//set enable bit of LP55231 to 0
  i2cWrite(LP55231_I2C, LP55231_REG_CNTRL1, 0x00);
  //Serial.println("LP55231 disabled.");
}

void LP55231_reset(){
//set reset register of LP55231
  i2cWrite(LP55231_I2C, LP55231_REG_RESET, 0xff);
}

/*void LP55231_enableRatiometricDimming(){
  //enable ratiometric dimming for D9
  i2cWrite(LP55231_I2C, LP55231_REG_RATIOMETRIC_DIMMING_MSB, 1);
  //enable ratiometric dimming for D9
  i2cWrite(LP55231_I2C, LP55231_REG_RATIOMETRIC_DIMMING_LSB, 0b01110001);
}*/

void LP55231_lightRed(uint8_t pwm, uint8_t current){
//light red LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D9_PWM, pwm);

  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D9_I_CTL, current);
}

void LP55231_lightGreen(uint8_t pwm, uint8_t current){
//light green LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D5_PWM, pwm);

  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D5_I_CTL, current);
}

void LP55231_lightBlue(uint8_t pwm, uint8_t current){
//light blue LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D6_PWM, pwm);

  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D6_I_CTL, current);
}

void LP55231_lightUV(uint8_t pwm, uint8_t current){
//light UV LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D8_PWM, pwm);

  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D8_I_CTL, current);
}

void LP55231_lightIR(uint8_t pwm, uint8_t current){
//light IR LED with given PWM and Current
  //write PWM duty cylcle value to PWM register
  i2cWrite(LP55231_I2C, LP55231_REG_D7_PWM, pwm);

  //write output current value to current control register
  i2cWrite(LP55231_I2C, LP55231_REG_D7_I_CTL, current);
}

void LP55231_ledsOff(){
//set current and PWM duty cycle of all LEDs to 0
  i2cWrite(LP55231_I2C, LP55231_REG_D9_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D9_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D5_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D5_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D6_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D6_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D8_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D8_PWM, 0);  
  i2cWrite(LP55231_I2C, LP55231_REG_D7_I_CTL, 0);
  i2cWrite(LP55231_I2C, LP55231_REG_D7_PWM, 0);  

 // Serial.println("Turned off all LEDs");
}

//***********************************************************
//*                 TCS3472 Functions                       *
//***********************************************************

void TCS3472_enable(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime){
//set gain and integration time, power on and enable RGBC, set wait long to 0
//integrationTime = 0x00 = 256 integration cycles (700 ms)
//integrationTime = 0xFF = 1 integration cycle (2,4 ms)
//Time = (256 - integrationTime) * 2,4 ms
    
  //set PON and AEN 1
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_ENABLE, 0x03);
  //wait for RGBC initialisation
  delay(100);

  //set wait long to 0
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_CONFIG, 0x00);

  //set gain level
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_RGBC_GAIN, TCS3472_GAIN_LEVEL);
  //set integration time
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_RGBC_TIME, integrationTime);
  //set WTIME; 2.4ms/step; 0xFF = 2.4ms, 0x00 = 614 ms
  //i2cWrite(TCS3472_I2C, TCS3472_REPEATED_BYTE | TCS3472_WAIT_TIME, 0x00);

  //wait for one integration period
  //necessary when enabling and immediately reading values back
  delay(TCS3472_getIntegrationTime());

  //Serial.println("TCS3472 enabled.");
}

void TCS3472_disable(){
//set PON and AEN to 0
  i2cWrite(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_ENABLE, 0x00);
  //Serial.println("TCS3472 disabled.");  
}

void TCS3472_getRawR(uint16_t *r){
//get raw r value for given gain level and integration time

  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read r value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_RDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write r value to pointer
  *r = data[1]<<8 | data[0];

  //wait for integration time period
  //delay(TCS3472_getIntegrationTime());
}

void TCS3472_getRawG(uint16_t *g){
//get raw g value for given gain level and integration time

  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read g value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_GDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write g value to pointer
  *g = data[1]<<8 | data[0];

  //wait for integration time period
  //delay(TCS3472_getIntegrationTime());
}

void TCS3472_getRawB(uint16_t *b){
//get raw b value for given gain level and integration time

  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read b value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_BDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write b value to pointer
  *b = data[1]<<8 | data[0];

  //wait for integration time period
  //delay(TCS3472_getIntegrationTime());
}

void TCS3472_getRawC(uint16_t *c){
//get raw c value for given gain level and integration time

  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read c value from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_CDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 2);
  uint8_t data[2];
  for(uint8_t i = 0; i < 8; i++)
    data[i] = Wire.read();

  //write c value to pointer
  *c = data[1]<<8 | data[0];

  //wait for integration time period
  //delay(TCS3472_getIntegrationTime());
}

void TCS3472_getRawRGBC(uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *c){
//get r, g, b, c values for given gain level and integration time

  //wait for one integration period
  delay(TCS3472_getIntegrationTime());
  delay(250);

  //read r, g, b, c values from data register
  Wire.beginTransmission(TCS3472_I2C);
  Wire.write(TCS3472_COMMAND_BIT | TCS3472_CDATAL);
  Wire.endTransmission();
  Wire.requestFrom(TCS3472_I2C, 8);
  uint8_t data[8];
  for(uint8_t i = 0; i < 8; i++){
    data[i] = Wire.read();
   /* Serial.print("data[");
    Serial.print(i);
    Serial.print("]: ");
    Serial.println(data[i]); */
  }

  //calculate r, g, b, c from data
  *c = data[1]<<8 | data[0];
  *r = data[3]<<8 | data[2];
  *g = data[5]<<8 | data[4];
  *b = data[7]<<8 | data[6];

  //wait for integration time period
  //delay(TCS3472_getIntegrationTime());
}

uint16_t TCS3472_getIntegrationTime(){
  uint8_t regValue = 0;
  regValue = i2cRead(TCS3472_I2C, TCS3472_COMMAND_BIT | TCS3472_RGBC_TIME, 1);

  uint16_t integrationTime = 0;
  integrationTime = (256 - regValue) * 2.4;

  return integrationTime;
}

void TCS3472_getR(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *r){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawR(r);
  TCS3472_disable();
}

void TCS3472_getG(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *g){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawG(g);
  TCS3472_disable();
}

void TCS3472_getB(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *b){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawB(b);
  TCS3472_disable();
}

void TCS3472_getC(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *c){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawC(c);
  TCS3472_disable();
}

void TCS3472_getRGBC(uint8_t TCS3472_GAIN_LEVEL, uint8_t integrationTime, uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *c){
//initilialise TCA3472, enables PON and RGBC, reads data and disables PON and RGBC
  TCS3472_enable(TCS3472_GAIN_LEVEL, integrationTime);
  TCS3472_getRawRGBC(r, g, b, c);
  TCS3472_disable();
}

/*
uint16_t TCS3472_calculateLux(uint16_t r, uint16_t g, uint16_t b){
  float lux;

  lux = (-0.32466F * r) + (1.57837F * g) + (-0.73191F * b);
  return (uint16_t)lux;
}*/
//***********************************************************
//*                 TSL2572 Functions                       *
//***********************************************************

void TSL2572_enable(uint8_t TSL2572_GAIN_LEVEL, uint8_t integrationTime){
//power on and enable ALS, set wait long and ALS gain level (1/6) to 0
//integrationTime = 0x00 = 256 integration cycles (699 ms)
//integrationTime = 0xFF = 1 integration cycle (2,73 ms)
//Time = (256 - integrationTime) * 2,73 ms

  //set wait long and ALS gain level (1/6) to 0
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_CONFIG, 0x00);
  
  //set gain level
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_GAIN, TSL2572_GAIN_LEVEL);
  //int regValue = i2cRead(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_GAIN, 1);
  //Serial.print("regValue gain enable: "); Serial.println(regValue);
  
  //check if gain 1/6 is enabled and set if true
  if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_1X))
    i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_CONFIG, 0x04);
  else if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_8X))
    i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_CONFIG, 0x04);
  else if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_16X)){
    Serial.println("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_16x");
    Serial2.println("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_16x");
    logToSD("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_16x");
    return;  
  }
  else if(_TSL2572_gainDivide_6 && (TSL2572_GAIN_LEVEL == TSL2572_GAIN_120X)){
    Serial.println("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_120x");
    Serial2.println("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_120x");
    logToSD("TSL2572 Error, don't use Gain/6 with TSL2572_GAIN_120x");
    return;   
  }

  //set integration time
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_TIME, integrationTime);
  //regValue = i2cRead(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_TIME, 1);
  //Serial.print("regValue intTime enable: "); Serial.println(regValue);
  
  //power on and enable ALS
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ENABLE, 0x03);
  //wait for ALS initialisation
  delay(3);
  
  //Serial.println("TSL2572 enabled.");
}

void TSL2572_disable(){
//power off and disable ALS
  i2cWrite(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ENABLE, 0x00);
 // Serial.println("TSL2572 disabled.");
}

uint16_t TSL2572_getRawData(uint8_t TSL2572_GAIN_LEVEL){
//get IR value for given gain level and integration time

  //wait for one integration period
  delay((uint16_t)TSL2572_getIntegrationTime());
  delay(250);
  
  //read IR measurements from data registers
  Wire.beginTransmission(TSL2572_I2C);
  Wire.write(TSL2572_AUTO_INCREMENT | TSL2572_DATA);
  Wire.endTransmission();
  Wire.requestFrom(TSL2572_I2C, 4);
  uint8_t data[4];
  for(uint8_t i = 0; i < 4; i++){
    data[i] = Wire.read();
    /*Serial.print("data[");
    Serial.print(i);
    Serial.print("]: ");
    Serial.println(data[i]);*/
  }

  //calculate c0, c1 from data
  uint16_t c0 = 0, c1 = 0;  
  c0 = data[1]<<8 | data[0];
  c1 = data[3]<<8 | data[2];
  /*
  Serial.print("c0: ");
  Serial.println(c0);
  Serial.print("c1: ");
  Serial.println(c1);
  */
  
  return max(c0, c1);

  /*
  //calculate gain value
  int gainValue = 0;
  if(TSL2572_GAIN_LEVEL == TSL2572_GAIN_1X) gainValue = 1;
  else if(TSL2572_GAIN_LEVEL == TSL2572_GAIN_8X) gainValue = 8;
  else if(TSL2572_GAIN_LEVEL == TSL2572_GAIN_16X) gainValue = 16;
  else if(TSL2572_GAIN_LEVEL == TSL2572_GAIN_120X) gainValue = 120;

  //calculate IR value according to datasheet
  float cpl = 0, lux1 = 0, lux2 = 0;
  cpl = (TSL2572_getIntegrationTime()) * (float)gainValue / 60.0; //counts per lux; falls kein Glas, muss Divisor korrigiert werden
  if(_TSL2572_gainDivide_6) 
    cpl /= 6.0;
  lux1 = ((float)c0 - (1.87 * (float)c1)) / cpl; 
  lux2 = ((0.63 * (float)c0) - (float)c1) / cpl;

  //calculate and return result
  float result = 0;
  result = max(lux1, lux2);
  //Serial.print("max(lux1, lux2): ");
  //Serial.println(result);
  return max(result, 0.0);
  */
}

float TSL2572_getIntegrationTime(){
  uint8_t regValue = 0;
  regValue = i2cRead(TSL2572_I2C, TSL2572_COMMAND_BIT | TSL2572_ALS_TIME, 1);

  float integrationTime = 0;
  integrationTime = (256 - regValue) * 2.73;

  return integrationTime;
}

void TSL2572_getIR(uint8_t TSL2572_GAIN_LEVEL, uint8_t integrationTime, uint16_t *ir){
//initilialise TSL2572, enables PON and ALS, reads data and disables PON and ALS
  TSL2572_enable(TSL2572_GAIN_LEVEL, integrationTime);
  *ir = TSL2572_getRawData(TSL2572_GAIN_LEVEL);
  TSL2572_disable();
}


//***********************************************************
//*                 VEML6070 Functions                      *
//***********************************************************

bool VEML6070_clearAck(){
    return i2cRead(VEML6070_ADDR_ARA, 1);
}

void VEML6070_enable(){
//set inital values for reserved bits and set SD bit to 0
  _VEML6070_command_register = 0x02;
   
  VEML6070_clearAck();
  i2cWrite(VEML6070_I2C_L, _VEML6070_command_register);
  
  //Serial.print("_VEML6070_command_register: ");
  //Serial.println(_VEML6070_command_register);
  //Serial.println("Initial values set. VEML6070 Shutdown disabled");  

 /* Serial.print("VEML command register: "); 
  uint16_t reg = i2cRead(VEML6070_I2C_L, 1); 
  Serial.println(reg); */

  delay(100);
}

void VEML6070_disable(){
//sets SD bit to 1
  _VEML6070_command_register |= VEML6070_SD_DISABLE;
  i2cWrite(VEML6070_I2C_L, _VEML6070_command_register);
  /* Serial.print("_VEML6070_command_register: ");
  Serial.println(_VEML6070_command_register); */
  
  //Serial.println("VEML6070 Shutdown enabled");
  
  /* Serial.print("VEML command register: ");  
  uint8_t reg = i2cRead(VEML6070_I2C_L, 1); 
  Serial.println(reg); */
}

uint16_t VEML6070_getRawData(uint8_t INTEGRATION_TIME){
//returns UV value
  //set integration time  
  _VEML6070_command_register |= INTEGRATION_TIME;
  i2cWrite(VEML6070_I2C_L, _VEML6070_command_register);
    
  //wait for one integration period
  VEML6070_wait(INTEGRATION_TIME);
  VEML6070_wait(INTEGRATION_TIME);
  //delay(250);

  //read values and calculate result
  uint16_t result = i2cRead(VEML6070_I2C_H, 1);
 
  result <<= 8;
  result |= i2cRead(VEML6070_I2C_L, 1);

  return result;
}

void VEML6070_wait(uint8_t INTEGRATION_TIME){
//wait for integration
  if(INTEGRATION_TIME == VEML6070_INT_HALF)
    delay(63);  //minimal integration time for 300k resistor
    //delay(126); //minimal integration time for 600k resistor
  else if(INTEGRATION_TIME == VEML6070_INT_1)
    delay(125);  //minimal integration time for 300k resistor
    //delay(250); //minimal integration time for 600k resistor 
  else if(INTEGRATION_TIME == VEML6070_INT_2)
     delay(250);  //minimal integration time for 300k resistor
    //delay(500); //minimal integration time for 600k resistor 
  else if(INTEGRATION_TIME == VEML6070_INT_4)
     delay(500);  //minimal integration time for 300k resistor
    //delay(1000); //minimal integration time for 600k resistor     
}

void VEML6070_getUV(uint8_t INTEGRATION_TIME, uint16_t *uv){
//enable VEML6070, read data and disable
  VEML6070_enable();
  *uv = VEML6070_getRawData(INTEGRATION_TIME);
  VEML6070_disable();
}

//***********************************************************
//*                 MS5803-14BA Functions                   *
//***********************************************************

void MS5803_enable(){
//check calibriertration coefficients
  uint8_t i;
  
  //read coefficient values
  for(i = 0; i <= 7; i++){
      i2cWrite(MS5803_I2C, MS5803_CMD_PROM + (i*2));
      _MS5803_coefficients[i] = i2cRead(MS5803_I2C, 2);
  
     /* Serial.print("C");
      Serial.print(i);
      Serial.print("= ");
      Serial.println(_MS5803_coefficients[i]); */
  }
}

void MS5803_reset(){
//Reset MS5803
  i2cWrite(MS5803_I2C, MS5803_CMD_RESET);
  delay(10);
}

uint32_t MS5803_getADCconversion(uint8_t MEASUREMENT,uint8_t PRECISION){
//recieve ADC measurement from MS5803
  uint32_t result;
  uint8_t hByte = 0, mByte = 0, lByte = 0; 
  
  //ADC convertion command
  i2cWrite(MS5803_I2C, MS5803_CMD_ADC_CONVERT + MEASUREMENT + PRECISION);
  delay(15);

  //ADC read command
  i2cWrite(MS5803_I2C, MS5803_CMD_ADC_READ);
  Wire.requestFrom(MS5803_I2C, 3);

  while(Wire.available()){
    hByte = Wire.read();
    mByte = Wire.read();
    lByte = Wire.read();
  }

  /*
  Serial.print("hByte: "); Serial.println(hByte);
  Serial.print("mByte: "); Serial.println(mByte);
  Serial.print("lByte: "); Serial.println(lByte);
  */
  
  result = ((uint32_t)hByte << 16) + ((uint32_t)mByte << 8) + lByte;
  //result = i2cRead(MS5803_I2C, 3);

  return result;
}

void MS5803_getValues(uint8_t PRECISION){
//calculate Temperature and pressure values

  //get rawTemperature
  int32_t rawTemperature = MS5803_getADCconversion(TEMPERATURE, PRECISION);
  int32_t rawPressure = MS5803_getADCconversion(PRESSURE, PRECISION);

  /*
  Serial.print("\n");  
  Serial.print("raw Temperature: ");
  Serial.println(rawTemperature);
  Serial.print("raw Pressure: ");
  Serial.println(rawPressure); 
  */
  
  //variables for calculations
  int32_t calcTemperature;
  int32_t calcPressure;
  
  int32_t dT;

  //calculate first order of temperature
  dT = rawTemperature - ((int32_t)_MS5803_coefficients[5] << 8);
  calcTemperature = (((int64_t)dT * _MS5803_coefficients[6]) >> 23) + 2000;

  //calculate second order
  int64_t T2, OFF2, SENS2, OFF, SENS; //working variables
  
  if (calcTemperature < 2000){
  // If calcTemperature is below 20.0C
  
    T2 = 3 * (((int64_t)dT * dT) >> 33);
    OFF2 = 3 * ((calcTemperature - 2000) * (calcTemperature - 2000)) / 2;
    SENS2 = 5 * ((calcTemperature - 2000) * (calcTemperature - 2000)) / 8;
    
    if(calcTemperature < -1500){
    // If calcTemperature is below -15.0C
      OFF2 = OFF2 + 7 * ((calcTemperature + 1500) * (calcTemperature + 1500));
      SENS2 = SENS2 + 4 * ((calcTemperature + 1500) * (calcTemperature + 1500));
    }
   } 
  else{
  // If calcTemperature is above 20.0C
  
    T2 = 7 * ((uint64_t)dT * dT)/pow(2,37);
    OFF2 = ((calcTemperature - 2000) * (calcTemperature - 2000)) / 16;
    SENS2 = 0;
  }

  //apply the calculated offsets
   OFF = ((int64_t)_MS5803_coefficients[2] << 16) + (((_MS5803_coefficients[4] * (int64_t)dT)) >> 7);
   SENS = ((int64_t)_MS5803_coefficients[1] << 15) + (((_MS5803_coefficients[3] * (int64_t)dT)) >> 8);
  
  calcTemperature = calcTemperature - T2;
  OFF = OFF - OFF2;
  SENS = SENS - SENS2;

  //calculate pressure
  calcPressure = (((SENS * rawPressure) / 2097152) - OFF) / 32768;

  _temperature = calcTemperature;
  _pressure = calcPressure;  
}

float MS5803_getTemperature(uint8_t PRECISION){
  //calculate and return temperature

  float calcTemperature;
  MS5803_getValues(PRECISION);

  calcTemperature = _temperature / 100.0f;
  return calcTemperature;
}

float MS5803_getPressure(uint8_t PRECISION){
//calculate and return pressure
  MS5803_getValues(PRECISION);
  float calcPressure;

  calcPressure = _pressure;
  calcPressure = calcPressure / 10.0f;

  return calcPressure;
}
